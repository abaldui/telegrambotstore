﻿namespace TelegramBotStore
{
    partial class EditSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditSettings));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxHash = new System.Windows.Forms.TextBox();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.QiwiFilePath = new System.Windows.Forms.TextBox();
            this.numQiwiNumbersCount = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.ListOfQiwiNumbers = new System.Windows.Forms.Button();
            this.textBoxQiwiApi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxQiwiWallet = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonImage = new System.Windows.Forms.Button();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.openFileDialogImage = new System.Windows.Forms.OpenFileDialog();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.checkBoxIsGreeting = new System.Windows.Forms.CheckBox();
            this.textBoxGreeting = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.numericUpDownLimit = new System.Windows.Forms.NumericUpDown();
            this.textBoxCardTransfer = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxWEX = new System.Windows.Forms.CheckBox();
            this.textBoxWexSecret = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxWexApi = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxProxyType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxProxyPassword = new System.Windows.Forms.TextBox();
            this.textBoxProxyUsername = new System.Windows.Forms.TextBox();
            this.textBoxProxyPort = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxProxyHost = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxBlockChainAddress = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.checkBoxBlockChain = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDownDiscountBTC = new System.Windows.Forms.NumericUpDown();
            this.textBoxBlockChainApiKey = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxBlockChainXPub = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.radioButtonKZT = new System.Windows.Forms.RadioButton();
            this.radioButtonRUB = new System.Windows.Forms.RadioButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.buttonSpecimenRemove = new System.Windows.Forms.Button();
            this.buttonSpecimenAdd = new System.Windows.Forms.Button();
            this.pictureBoxSpecimen = new System.Windows.Forms.PictureBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.checkBoxCard = new System.Windows.Forms.CheckBox();
            this.textBoxCard = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQiwiNumbersCount)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLimit)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDiscountBTC)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecimen)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxHash);
            this.groupBox1.Controls.Add(this.textBoxId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxPhone);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 103);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Телеграм";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Api_id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Api_hash";
            // 
            // textBoxHash
            // 
            this.textBoxHash.Location = new System.Drawing.Point(64, 71);
            this.textBoxHash.Name = "textBoxHash";
            this.textBoxHash.Size = new System.Drawing.Size(190, 20);
            this.textBoxHash.TabIndex = 3;
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(64, 45);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.Size = new System.Drawing.Size(190, 20);
            this.textBoxId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Телефон";
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(64, 19);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(190, 20);
            this.textBoxPhone.TabIndex = 3;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.QiwiFilePath);
            this.groupBox4.Controls.Add(this.numQiwiNumbersCount);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.ListOfQiwiNumbers);
            this.groupBox4.Controls.Add(this.textBoxQiwiApi);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.textBoxQiwiWallet);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(12, 122);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(260, 84);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Qiwi";
            // 
            // button1
            // 
            this.button1.Image = global::TelegramBotStore.Properties.Resources.search;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(179, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 24);
            this.button1.TabIndex = 42;
            this.button1.Text = "Выбрать";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // QiwiFilePath
            // 
            this.QiwiFilePath.Location = new System.Drawing.Point(5, 20);
            this.QiwiFilePath.Margin = new System.Windows.Forms.Padding(2);
            this.QiwiFilePath.Name = "QiwiFilePath";
            this.QiwiFilePath.Size = new System.Drawing.Size(250, 20);
            this.QiwiFilePath.TabIndex = 41;
            // 
            // numQiwiNumbersCount
            // 
            this.numQiwiNumbersCount.Location = new System.Drawing.Point(94, 62);
            this.numQiwiNumbersCount.Margin = new System.Windows.Forms.Padding(2);
            this.numQiwiNumbersCount.Name = "numQiwiNumbersCount";
            this.numQiwiNumbersCount.Size = new System.Drawing.Size(62, 20);
            this.numQiwiNumbersCount.TabIndex = 40;
            this.numQiwiNumbersCount.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numQiwiNumbersCount.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Колво номеров";
            this.label14.Visible = false;
            // 
            // ListOfQiwiNumbers
            // 
            this.ListOfQiwiNumbers.Location = new System.Drawing.Point(65, 10);
            this.ListOfQiwiNumbers.Margin = new System.Windows.Forms.Padding(2);
            this.ListOfQiwiNumbers.Name = "ListOfQiwiNumbers";
            this.ListOfQiwiNumbers.Size = new System.Drawing.Size(121, 26);
            this.ListOfQiwiNumbers.TabIndex = 35;
            this.ListOfQiwiNumbers.Text = "Qiwi list";
            this.ListOfQiwiNumbers.UseVisualStyleBackColor = true;
            this.ListOfQiwiNumbers.Visible = false;
            this.ListOfQiwiNumbers.Click += new System.EventHandler(this.ListOfQiwiNumbers_Click);
            // 
            // textBoxQiwiApi
            // 
            this.textBoxQiwiApi.Location = new System.Drawing.Point(65, 32);
            this.textBoxQiwiApi.Name = "textBoxQiwiApi";
            this.textBoxQiwiApi.Size = new System.Drawing.Size(190, 20);
            this.textBoxQiwiApi.TabIndex = 3;
            this.textBoxQiwiApi.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Api";
            this.label2.Visible = false;
            // 
            // textBoxQiwiWallet
            // 
            this.textBoxQiwiWallet.Location = new System.Drawing.Point(65, 12);
            this.textBoxQiwiWallet.Name = "textBoxQiwiWallet";
            this.textBoxQiwiWallet.Size = new System.Drawing.Size(190, 20);
            this.textBoxQiwiWallet.TabIndex = 1;
            this.textBoxQiwiWallet.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Кошелек";
            this.label1.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDown1);
            this.groupBox3.Location = new System.Drawing.Point(547, 329);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(155, 69);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Время для оплаты заказа";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(9, 19);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(62, 20);
            this.numericUpDown1.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBoxIsGreeting);
            this.groupBox5.Controls.Add(this.buttonRemove);
            this.groupBox5.Controls.Add(this.buttonImage);
            this.groupBox5.Controls.Add(this.pictureBoxImage);
            this.groupBox5.Location = new System.Drawing.Point(13, 323);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(260, 208);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Изображение";
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemove.Image = global::TelegramBotStore.Properties.Resources.remove;
            this.buttonRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonRemove.Location = new System.Drawing.Point(179, 178);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(75, 24);
            this.buttonRemove.TabIndex = 2;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonImage
            // 
            this.buttonImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImage.Image = global::TelegramBotStore.Properties.Resources.search;
            this.buttonImage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonImage.Location = new System.Drawing.Point(179, 153);
            this.buttonImage.Name = "buttonImage";
            this.buttonImage.Size = new System.Drawing.Size(75, 24);
            this.buttonImage.TabIndex = 1;
            this.buttonImage.Text = "Выбрать";
            this.buttonImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonImage.UseVisualStyleBackColor = true;
            this.buttonImage.Click += new System.EventHandler(this.buttonImage_Click);
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxImage.Location = new System.Drawing.Point(10, 19);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(164, 183);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxImage.TabIndex = 0;
            this.pictureBoxImage.TabStop = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Image = ((System.Drawing.Image)(resources.GetObject("buttonCancel.Image")));
            this.buttonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCancel.Location = new System.Drawing.Point(683, 501);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 24);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Image = ((System.Drawing.Image)(resources.GetObject("buttonOk.Image")));
            this.buttonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonOk.Location = new System.Drawing.Point(764, 501);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(88, 24);
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Text = "Сохранить";
            this.buttonOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // openFileDialogImage
            // 
            this.openFileDialogImage.FileName = "openFileDialog1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxGreeting);
            this.groupBox6.Location = new System.Drawing.Point(12, 215);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(526, 102);
            this.groupBox6.TabIndex = 25;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Приветствие";
            // 
            // checkBoxIsGreeting
            // 
            this.checkBoxIsGreeting.AutoSize = true;
            this.checkBoxIsGreeting.Location = new System.Drawing.Point(180, 130);
            this.checkBoxIsGreeting.Name = "checkBoxIsGreeting";
            this.checkBoxIsGreeting.Size = new System.Drawing.Size(61, 17);
            this.checkBoxIsGreeting.TabIndex = 5;
            this.checkBoxIsGreeting.Text = "Испол.";
            this.checkBoxIsGreeting.UseVisualStyleBackColor = true;
            // 
            // textBoxGreeting
            // 
            this.textBoxGreeting.Location = new System.Drawing.Point(9, 19);
            this.textBoxGreeting.Multiline = true;
            this.textBoxGreeting.Name = "textBoxGreeting";
            this.textBoxGreeting.Size = new System.Drawing.Size(511, 77);
            this.textBoxGreeting.TabIndex = 4;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.numericUpDownLimit);
            this.groupBox7.Controls.Add(this.textBoxCardTransfer);
            this.groupBox7.Location = new System.Drawing.Point(547, 404);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(314, 48);
            this.groupBox7.TabIndex = 26;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Достижение лимита/Перевод на карту";
            // 
            // numericUpDownLimit
            // 
            this.numericUpDownLimit.Location = new System.Drawing.Point(9, 19);
            this.numericUpDownLimit.Name = "numericUpDownLimit";
            this.numericUpDownLimit.Size = new System.Drawing.Size(93, 20);
            this.numericUpDownLimit.TabIndex = 3;
            // 
            // textBoxCardTransfer
            // 
            this.textBoxCardTransfer.Location = new System.Drawing.Point(108, 19);
            this.textBoxCardTransfer.Name = "textBoxCardTransfer";
            this.textBoxCardTransfer.Size = new System.Drawing.Size(197, 20);
            this.textBoxCardTransfer.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxWEX);
            this.groupBox2.Controls.Add(this.textBoxWexSecret);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBoxWexApi);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(279, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 103);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WEX";
            // 
            // checkBoxWEX
            // 
            this.checkBoxWEX.AutoSize = true;
            this.checkBoxWEX.Location = new System.Drawing.Point(7, 16);
            this.checkBoxWEX.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxWEX.Name = "checkBoxWEX";
            this.checkBoxWEX.Size = new System.Drawing.Size(107, 17);
            this.checkBoxWEX.TabIndex = 5;
            this.checkBoxWEX.Text = "Is WEX available";
            this.checkBoxWEX.UseVisualStyleBackColor = true;
            this.checkBoxWEX.CheckedChanged += new System.EventHandler(this.checkBoxWEX_CheckedChanged);
            // 
            // textBoxWexSecret
            // 
            this.textBoxWexSecret.Location = new System.Drawing.Point(62, 74);
            this.textBoxWexSecret.Name = "textBoxWexSecret";
            this.textBoxWexSecret.Size = new System.Drawing.Size(190, 20);
            this.textBoxWexSecret.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Secret";
            // 
            // textBoxWexApi
            // 
            this.textBoxWexApi.Location = new System.Drawing.Point(62, 48);
            this.textBoxWexApi.Name = "textBoxWexApi";
            this.textBoxWexApi.Size = new System.Drawing.Size(190, 20);
            this.textBoxWexApi.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "API";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.comboBoxProxyType);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Controls.Add(this.textBoxProxyPassword);
            this.groupBox8.Controls.Add(this.textBoxProxyUsername);
            this.groupBox8.Controls.Add(this.textBoxProxyPort);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Controls.Add(this.textBoxProxyHost);
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Location = new System.Drawing.Point(547, 170);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(314, 153);
            this.groupBox8.TabIndex = 30;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Прокси";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Тип";
            // 
            // comboBoxProxyType
            // 
            this.comboBoxProxyType.FormattingEnabled = true;
            this.comboBoxProxyType.Items.AddRange(new object[] {
            "HTTP",
            "SOCKS4",
            "SOCKS4A",
            "SOCKS5",
            "CHAIN"});
            this.comboBoxProxyType.Location = new System.Drawing.Point(75, 126);
            this.comboBoxProxyType.Name = "comboBoxProxyType";
            this.comboBoxProxyType.Size = new System.Drawing.Size(223, 21);
            this.comboBoxProxyType.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Пароль";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Логин";
            // 
            // textBoxProxyPassword
            // 
            this.textBoxProxyPassword.Location = new System.Drawing.Point(75, 97);
            this.textBoxProxyPassword.Name = "textBoxProxyPassword";
            this.textBoxProxyPassword.Size = new System.Drawing.Size(223, 20);
            this.textBoxProxyPassword.TabIndex = 5;
            // 
            // textBoxProxyUsername
            // 
            this.textBoxProxyUsername.Location = new System.Drawing.Point(75, 70);
            this.textBoxProxyUsername.Name = "textBoxProxyUsername";
            this.textBoxProxyUsername.Size = new System.Drawing.Size(223, 20);
            this.textBoxProxyUsername.TabIndex = 4;
            // 
            // textBoxProxyPort
            // 
            this.textBoxProxyPort.Location = new System.Drawing.Point(75, 42);
            this.textBoxProxyPort.Name = "textBoxProxyPort";
            this.textBoxProxyPort.Size = new System.Drawing.Size(223, 20);
            this.textBoxProxyPort.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Порт";
            // 
            // textBoxProxyHost
            // 
            this.textBoxProxyHost.Location = new System.Drawing.Point(75, 16);
            this.textBoxProxyHost.Name = "textBoxProxyHost";
            this.textBoxProxyHost.Size = new System.Drawing.Size(223, 20);
            this.textBoxProxyHost.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Хост";
            // 
            // textBoxBlockChainAddress
            // 
            this.textBoxBlockChainAddress.Location = new System.Drawing.Point(76, 40);
            this.textBoxBlockChainAddress.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxBlockChainAddress.Name = "textBoxBlockChainAddress";
            this.textBoxBlockChainAddress.Size = new System.Drawing.Size(223, 20);
            this.textBoxBlockChainAddress.TabIndex = 31;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.checkBoxBlockChain);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.numericUpDownDiscountBTC);
            this.groupBox9.Controls.Add(this.textBoxBlockChainApiKey);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.textBoxBlockChainXPub);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Controls.Add(this.label13);
            this.groupBox9.Controls.Add(this.textBoxBlockChainAddress);
            this.groupBox9.Location = new System.Drawing.Point(544, 12);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox9.Size = new System.Drawing.Size(314, 153);
            this.groupBox9.TabIndex = 33;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Blockchain";
            // 
            // checkBoxBlockChain
            // 
            this.checkBoxBlockChain.AutoSize = true;
            this.checkBoxBlockChain.Location = new System.Drawing.Point(10, 16);
            this.checkBoxBlockChain.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxBlockChain.Name = "checkBoxBlockChain";
            this.checkBoxBlockChain.Size = new System.Drawing.Size(136, 17);
            this.checkBoxBlockChain.TabIndex = 42;
            this.checkBoxBlockChain.Text = "Is BlockChain available";
            this.checkBoxBlockChain.UseVisualStyleBackColor = true;
            this.checkBoxBlockChain.Click += new System.EventHandler(this.checkBoxBlockChain_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 121);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 41;
            this.label18.Text = "Скидка";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(143, 122);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 13);
            this.label17.TabIndex = 40;
            this.label17.Text = "%";
            // 
            // numericUpDownDiscountBTC
            // 
            this.numericUpDownDiscountBTC.Location = new System.Drawing.Point(77, 118);
            this.numericUpDownDiscountBTC.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownDiscountBTC.Name = "numericUpDownDiscountBTC";
            this.numericUpDownDiscountBTC.Size = new System.Drawing.Size(62, 20);
            this.numericUpDownDiscountBTC.TabIndex = 39;
            this.numericUpDownDiscountBTC.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // textBoxBlockChainApiKey
            // 
            this.textBoxBlockChainApiKey.Location = new System.Drawing.Point(77, 94);
            this.textBoxBlockChainApiKey.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxBlockChainApiKey.Name = "textBoxBlockChainApiKey";
            this.textBoxBlockChainApiKey.Size = new System.Drawing.Size(223, 20);
            this.textBoxBlockChainApiKey.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 97);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "ApiKey";
            // 
            // textBoxBlockChainXPub
            // 
            this.textBoxBlockChainXPub.Location = new System.Drawing.Point(77, 70);
            this.textBoxBlockChainXPub.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxBlockChainXPub.Name = "textBoxBlockChainXPub";
            this.textBoxBlockChainXPub.Size = new System.Drawing.Size(223, 20);
            this.textBoxBlockChainXPub.TabIndex = 36;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 72);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "xPub";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 42);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Адресс";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.radioButtonKZT);
            this.groupBox10.Controls.Add(this.radioButtonRUB);
            this.groupBox10.Location = new System.Drawing.Point(716, 329);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox10.Size = new System.Drawing.Size(145, 69);
            this.groupBox10.TabIndex = 34;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выбор валюты";
            // 
            // radioButtonKZT
            // 
            this.radioButtonKZT.AutoSize = true;
            this.radioButtonKZT.Location = new System.Drawing.Point(5, 47);
            this.radioButtonKZT.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonKZT.Name = "radioButtonKZT";
            this.radioButtonKZT.Size = new System.Drawing.Size(46, 17);
            this.radioButtonKZT.TabIndex = 1;
            this.radioButtonKZT.TabStop = true;
            this.radioButtonKZT.Text = "KZT";
            this.radioButtonKZT.UseVisualStyleBackColor = true;
            // 
            // radioButtonRUB
            // 
            this.radioButtonRUB.AutoSize = true;
            this.radioButtonRUB.Location = new System.Drawing.Point(4, 24);
            this.radioButtonRUB.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonRUB.Name = "radioButtonRUB";
            this.radioButtonRUB.Size = new System.Drawing.Size(48, 17);
            this.radioButtonRUB.TabIndex = 0;
            this.radioButtonRUB.Text = "RUB";
            this.radioButtonRUB.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.buttonSpecimenRemove);
            this.groupBox11.Controls.Add(this.buttonSpecimenAdd);
            this.groupBox11.Controls.Add(this.pictureBoxSpecimen);
            this.groupBox11.Location = new System.Drawing.Point(279, 323);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(258, 208);
            this.groupBox11.TabIndex = 35;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Проба";
            // 
            // buttonSpecimenRemove
            // 
            this.buttonSpecimenRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSpecimenRemove.Image = global::TelegramBotStore.Properties.Resources.remove;
            this.buttonSpecimenRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSpecimenRemove.Location = new System.Drawing.Point(177, 178);
            this.buttonSpecimenRemove.Name = "buttonSpecimenRemove";
            this.buttonSpecimenRemove.Size = new System.Drawing.Size(75, 24);
            this.buttonSpecimenRemove.TabIndex = 2;
            this.buttonSpecimenRemove.Text = "Удалить";
            this.buttonSpecimenRemove.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSpecimenRemove.UseVisualStyleBackColor = true;
            this.buttonSpecimenRemove.Click += new System.EventHandler(this.buttonSpecimenRemove_Click);
            // 
            // buttonSpecimenAdd
            // 
            this.buttonSpecimenAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSpecimenAdd.Image = global::TelegramBotStore.Properties.Resources.search;
            this.buttonSpecimenAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSpecimenAdd.Location = new System.Drawing.Point(178, 153);
            this.buttonSpecimenAdd.Name = "buttonSpecimenAdd";
            this.buttonSpecimenAdd.Size = new System.Drawing.Size(75, 24);
            this.buttonSpecimenAdd.TabIndex = 1;
            this.buttonSpecimenAdd.Text = "Выбрать";
            this.buttonSpecimenAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSpecimenAdd.UseVisualStyleBackColor = true;
            this.buttonSpecimenAdd.Click += new System.EventHandler(this.buttonSpecimenAdd_Click);
            // 
            // pictureBoxSpecimen
            // 
            this.pictureBoxSpecimen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxSpecimen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxSpecimen.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxSpecimen.Name = "pictureBoxSpecimen";
            this.pictureBoxSpecimen.Size = new System.Drawing.Size(165, 183);
            this.pictureBoxSpecimen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSpecimen.TabIndex = 0;
            this.pictureBoxSpecimen.TabStop = false;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.checkBoxCard);
            this.groupBox12.Controls.Add(this.textBoxCard);
            this.groupBox12.Controls.Add(this.label20);
            this.groupBox12.Location = new System.Drawing.Point(279, 122);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(261, 85);
            this.groupBox12.TabIndex = 36;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Карта";
            // 
            // checkBoxCard
            // 
            this.checkBoxCard.AutoSize = true;
            this.checkBoxCard.Location = new System.Drawing.Point(7, 16);
            this.checkBoxCard.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxCard.Name = "checkBoxCard";
            this.checkBoxCard.Size = new System.Drawing.Size(104, 17);
            this.checkBoxCard.TabIndex = 5;
            this.checkBoxCard.Text = "Is Card available";
            this.checkBoxCard.UseVisualStyleBackColor = true;
            this.checkBoxCard.Click += new System.EventHandler(this.checkBoxCard_Click);
            // 
            // textBoxCard
            // 
            this.textBoxCard.Location = new System.Drawing.Point(47, 36);
            this.textBoxCard.Name = "textBoxCard";
            this.textBoxCard.Size = new System.Drawing.Size(206, 20);
            this.textBoxCard.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 43);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Карта";
            // 
            // EditSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 539);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.EditSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQiwiNumbersCount)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLimit)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDiscountBTC)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecimen)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.TextBox textBoxHash;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxQiwiApi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxQiwiWallet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button buttonImage;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.OpenFileDialog openFileDialogImage;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBoxGreeting;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown numericUpDownLimit;
        private System.Windows.Forms.TextBox textBoxCardTransfer;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxWexSecret;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxWexApi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxProxyType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxProxyPassword;
        private System.Windows.Forms.TextBox textBoxProxyUsername;
        private System.Windows.Forms.TextBox textBoxProxyPort;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxProxyHost;
        private System.Windows.Forms.TextBox textBoxBlockChainAddress;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxBlockChainXPub;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxBlockChainApiKey;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numericUpDownDiscountBTC;
        private System.Windows.Forms.CheckBox checkBoxBlockChain;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton radioButtonKZT;
        private System.Windows.Forms.RadioButton radioButtonRUB;
        private System.Windows.Forms.CheckBox checkBoxWEX;
        private System.Windows.Forms.Button ListOfQiwiNumbers;
        private System.Windows.Forms.NumericUpDown numQiwiNumbersCount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox QiwiFilePath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button buttonSpecimenRemove;
        private System.Windows.Forms.Button buttonSpecimenAdd;
        private System.Windows.Forms.PictureBox pictureBoxSpecimen;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox checkBoxCard;
        private System.Windows.Forms.TextBox textBoxCard;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox checkBoxIsGreeting;
    }
}