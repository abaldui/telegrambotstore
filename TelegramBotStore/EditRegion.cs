﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;

namespace TelegramBotStore
{
    public partial class EditRegion : Form
    {
        private readonly ILog log = LogManager.GetLogger("TelegramBotSushi.EditRegion.cs");

        private BotContext context;
        private City city;
        private Domain.Region region;

        private long cityId;
        private long? regionId;

        public EditRegion(long _cityId, long? _regionId)
        {
            InitializeComponent();

            cityId = _cityId;
            regionId = _regionId;
        }

        private void EditRegion_Load(object sender, EventArgs e)
        {
            context = BotContext.Create();

            city = context.Cities.FirstOrDefault(x => x.Id == cityId);

            if (regionId.HasValue)
            {
                region = city.Regions.FirstOrDefault(x => x.Id == regionId);
                textBoxName.Text = region.Name;
            }

            this.Text = regionId.HasValue ? "Редактирование района" : "Добавление района";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxName.Text))
                return;

            if (!regionId.HasValue && city.Regions.Any(x => x.Name == textBoxName.Text))
            {
                MessageBox.Show("Район с таким название уже добавлен", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                if (!regionId.HasValue)
                {
                    string[] separator = new string[] { "\r\n" };
                    string[] splits = textBoxName.Text.Split(separator, StringSplitOptions.None);

                    foreach (string split in splits)
                    {
                        if (!city.Regions.Any(x => x.Name == split)) city.Regions.Add(new Domain.Region(split));
                    }
                }
                else
                    region.Name = textBoxName.Text;

                context.SaveChanges();

                Singleton.Main.InitRegions();

                this.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
