﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TelegramBotStore.Domain.DbContext;

namespace TelegramBotStore
{
    public partial class EditMenu : Form
    {
        private BotContext context = null;
        private Domain.Menu _selectedMenu;

        public EditMenu()
        {
            InitializeComponent();
        }

        private void EditMenu_Load(object sender, EventArgs e)
        {
            context = BotContext.Create();

            initMenu();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBoxAll_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] separators = new string[] { " — " };
            string[] splits = comboBoxAll.SelectedItem.ToString().Split(separators, StringSplitOptions.RemoveEmptyEntries);
            string sign = splits[0];

            _selectedMenu = context.Menu.FirstOrDefault(x => x.Sign == sign);

            textBoxSign.Text = _selectedMenu.Sign;
            textBoxName.Text = _selectedMenu.Name;
            textBoxPage.Text = _selectedMenu.Page;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxSign.Text) || String.IsNullOrEmpty(textBoxName.Text) || String.IsNullOrEmpty(textBoxPage.Text))
            {
                MessageBox.Show("Заполните информацию", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (context.Menu.Any(x => x.Sign == textBoxSign.Text))
            {
                MessageBox.Show("Меню с таким знаком уже добавлено", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Domain.Menu menu = new Domain.Menu();
            menu.Sign = textBoxSign.Text;
            menu.Name = textBoxName.Text;
            menu.Page = textBoxPage.Text;

            context.Menu.Add(menu);
            context.SaveChanges();

            initMenu();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (_selectedMenu != null)
            {
                context.Menu.Remove(_selectedMenu);
                context.SaveChanges();

                _selectedMenu = null;

                initMenu();
            }
        }

        private void initMenu()
        {
            comboBoxAll.Items.Clear();

            List<Domain.Menu> list = context.Menu.ToList();

            foreach (Domain.Menu menu in list)
            {
                comboBoxAll.Items.Add(menu.Sign + " — " + menu.Name);
            }

            if (comboBoxAll.Items.Count > 0)
                comboBoxAll.SelectedIndex = 0;
            else
                textBoxSign.Text = textBoxName.Text = textBoxPage.Text = String.Empty;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (_selectedMenu != null)
            {
                if (_selectedMenu.Sign != textBoxSign.Text && context.Menu.Any(x => x.Sign == textBoxSign.Text))
                {
                    if (context.Menu.Any(x => x.Sign == textBoxSign.Text))
                    {
                        MessageBox.Show("Меню с таким знаком уже добавлено", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                _selectedMenu.Sign = textBoxSign.Text;
                _selectedMenu.Name = textBoxName.Text;
                _selectedMenu.Page = textBoxPage.Text;

                context.SaveChanges();

                initMenu();
            }
        }
    }
}
