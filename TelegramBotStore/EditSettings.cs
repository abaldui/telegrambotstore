﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace TelegramBotStore
{
    public partial class EditSettings : Form
    {
        public static string SPECIMEN_PHOTO = "Specimen.jpeg";

        private string PATH = Directory.GetCurrentDirectory() + @"\Images\";

        public EditSettings()
        {
            InitializeComponent();
        }

        private void EditSettings_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.TimeToWait == 0)
            {
                Properties.Settings.Default.TimeToWait = 1;
                Properties.Settings.Default.Save();
            }

            textBoxPhone.Text = Properties.Settings.Default.Phone;
            textBoxId.Text = Properties.Settings.Default.Api_Id != 0 ? Properties.Settings.Default.Api_Id.ToString() : String.Empty;
            textBoxHash.Text = Properties.Settings.Default.Api_Hash;
            textBoxQiwiWallet.Text = Properties.Settings.Default.QiwiWallet;
            textBoxQiwiApi.Text = Properties.Settings.Default.QiwiApi;
            numericUpDown1.Value = Properties.Settings.Default.TimeToWait;
            textBoxGreeting.Text = Properties.Settings.Default.Greeting;
            numericUpDownLimit.Value = Properties.Settings.Default.Limit;
            textBoxCardTransfer.Text = Properties.Settings.Default.CardTransfer;

            pictureBoxImage.ImageLocation = PATH + "greeting.jpeg";
            pictureBoxSpecimen.ImageLocation = PATH + SPECIMEN_PHOTO;

            textBoxWexApi.Text = Properties.Settings.Default.API;
            textBoxWexSecret.Text = Properties.Settings.Default.SECRET;

            textBoxProxyHost.Text = Properties.Settings.Default.ProxyHost;
            textBoxProxyPort.Text = Properties.Settings.Default.ProxyPort.ToString();
            textBoxProxyUsername.Text = Properties.Settings.Default.ProxyUsername;
            textBoxProxyPassword.Text = Properties.Settings.Default.ProxyPassword;
            comboBoxProxyType.SelectedIndex = Properties.Settings.Default.ProxyType;

            textBoxBlockChainAddress.Text = Properties.Settings.Default.BblockChainAdderess;
            textBoxBlockChainApiKey.Text = Properties.Settings.Default.BlockChainApiKey;
            textBoxBlockChainXPub.Text = Properties.Settings.Default.BlockChainXPub;
            numericUpDownDiscountBTC.Value = Properties.Settings.Default.DiscountBTC;

            textBoxCard.Text = Properties.Settings.Default.CardNumber;

            textBoxWexApi.Enabled = textBoxWexSecret.Enabled
                = checkBoxWEX.Checked = Properties.Settings.Default.IsWexAvailable;
            textBoxBlockChainAddress.Enabled = textBoxBlockChainApiKey.Enabled = textBoxBlockChainXPub.Enabled = numericUpDownDiscountBTC.Enabled
                = checkBoxBlockChain.Checked = Properties.Settings.Default.IsBlockChainAvailable;
            textBoxCard.Enabled = checkBoxCard.Checked = Properties.Settings.Default.IsCardAvailable;

            numQiwiNumbersCount.Value = Properties.Settings.Default.QiwiNumbersCount;
            QiwiFilePath.Text = Properties.Settings.Default.PATH_QIWI_FILE;

            checkBoxIsGreeting.Checked = Properties.Settings.Default.IsGreeting;

            if (Properties.Settings.Default.Currency == 0)
            {
                radioButtonRUB.Checked = true;
            }
            else if (Properties.Settings.Default.Currency == 1)
            {
                radioButtonKZT.Checked = true;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            int.TryParse(textBoxId.Text, out int id);
            Properties.Settings.Default.Api_Id = id;
            Properties.Settings.Default.Phone = textBoxPhone.Text;
            Properties.Settings.Default.Api_Hash = textBoxHash.Text;

            Properties.Settings.Default.QiwiWallet = textBoxQiwiWallet.Text;
            Properties.Settings.Default.QiwiApi = textBoxQiwiApi.Text;
            Properties.Settings.Default.TimeToWait = int.Parse(numericUpDown1.Value.ToString());
            Properties.Settings.Default.Greeting = textBoxGreeting.Text;
            Properties.Settings.Default.Limit = numericUpDownLimit.Value;
            Properties.Settings.Default.CardTransfer = textBoxCardTransfer.Text;

            Properties.Settings.Default.API = textBoxWexApi.Text;
            Properties.Settings.Default.SECRET = textBoxWexSecret.Text;

            Properties.Settings.Default.ProxyHost = textBoxProxyHost.Text;
            Properties.Settings.Default.ProxyPort = int.Parse(textBoxProxyPort.Text);
            Properties.Settings.Default.ProxyUsername = textBoxProxyUsername.Text;
            Properties.Settings.Default.ProxyPassword = textBoxProxyPassword.Text;
            Properties.Settings.Default.ProxyType = comboBoxProxyType.SelectedIndex;

            Properties.Settings.Default.BblockChainAdderess = textBoxBlockChainAddress.Text;
            Properties.Settings.Default.BlockChainApiKey = textBoxBlockChainApiKey.Text;
            Properties.Settings.Default.BlockChainXPub = textBoxBlockChainXPub.Text;
            Properties.Settings.Default.DiscountBTC = numericUpDownDiscountBTC.Value;

            Properties.Settings.Default.CardNumber = textBoxCard.Text;

            Properties.Settings.Default.IsWexAvailable = checkBoxWEX.Checked;
            Properties.Settings.Default.IsBlockChainAvailable = checkBoxBlockChain.Checked;
            Properties.Settings.Default.IsCardAvailable = checkBoxCard.Checked;

            Properties.Settings.Default.QiwiNumbersCount = (int)numQiwiNumbersCount.Value;
            Properties.Settings.Default.PATH_QIWI_FILE = QiwiFilePath.Text;

            Properties.Settings.Default.IsGreeting = checkBoxIsGreeting.Checked;

            if (radioButtonRUB.Checked == true)
            {
                Properties.Settings.Default.Currency = 0;
            }
            else if (radioButtonKZT.Checked == true)
            {
                Properties.Settings.Default.Currency = 1;
            }

            Properties.Settings.Default.Save();

            if (!Directory.Exists(PATH))
                Directory.CreateDirectory(PATH);

            if (pictureBoxImage.Image != null)
            {
                pictureBoxImage.Image.Save(PATH + "greeting.jpeg", ImageFormat.Jpeg);
            }

            if (pictureBoxSpecimen.Image != null)
            {
                pictureBoxSpecimen.Image.Save(PATH + SPECIMEN_PHOTO, ImageFormat.Jpeg);
            }

            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBoxBlockChain_Click(object sender, EventArgs e)
        {
            textBoxBlockChainAddress.Enabled = textBoxBlockChainApiKey.Enabled = textBoxBlockChainXPub.Enabled = numericUpDownDiscountBTC.Enabled
                = checkBoxBlockChain.Checked;
        }

        private void checkBoxWEX_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWexApi.Enabled = textBoxWexSecret.Enabled
                            = checkBoxWEX.Checked;
        }

        private void checkBoxCard_Click(object sender, EventArgs e)
        {
            textBoxCard.Enabled = checkBoxCard.Checked;
        }

        private void ListOfQiwiNumbers_Click(object sender, EventArgs e)
        {
            FormListQiwiNumbers numbers = new FormListQiwiNumbers();
            numbers.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialog1.OpenFile() != null)
                        QiwiFilePath.Text = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void buttonImage_Click(object sender, EventArgs e)
        {
            if (openFileDialogImage.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialogImage.OpenFile() != null)
                        pictureBoxImage.Image = new Bitmap(openFileDialogImage.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            pictureBoxImage.Image = null;

            string path = PATH + "greeting.jpeg";
            if (File.Exists(path))
                File.Delete(path);
        }

        private void buttonSpecimenAdd_Click(object sender, EventArgs e)
        {
            if (openFileDialogImage.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (openFileDialogImage.OpenFile() != null)
                        pictureBoxSpecimen.Image = new Bitmap(openFileDialogImage.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void buttonSpecimenRemove_Click(object sender, EventArgs e)
        {
            pictureBoxSpecimen.Image = null;

            string path = PATH + SPECIMEN_PHOTO;
            if (File.Exists(path))
                File.Delete(path);
        }
    }
}
