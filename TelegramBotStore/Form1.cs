﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;

namespace TelegramBotStore
{
    public partial class Comment : Form
    {
        public Comment()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Review> listCom = new List<Review>();

            foreach (string item in richTextBoxComments.Lines)
            {
                string[] str = item.Split('=');
                if (str.Length != 2)
                {
                    MessageBox.Show("Проверьте правильность комментариев, в каждой строчке должно быть одно =.");
                    return;
                }
                else
                {
                    listCom.Add(new Review() { Text = str[1].Trim(), Din = DateTime.Now, From = str[0].Trim() });
                }
            }

            BotContext context = BotContext.Create();

            foreach (var rew in listCom)
            {
                context.Reviews.Add(rew);
            }
            
            context.SaveChanges();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
