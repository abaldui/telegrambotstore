﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;

namespace TelegramBotStore
{
    public partial class EditCity : Form
    {
        private readonly ILog log = LogManager.GetLogger("TelegramBotSushi.EditCity.cs");

        private BotContext context;
        private City city;

        private long? cityId;

        public EditCity(long? _cityId)
        {
            InitializeComponent();

            cityId = _cityId;
        }

        private void EditCity_Load(object sender, EventArgs e)
        {
            context = BotContext.Create();

            if (cityId.HasValue)
            {
                city = context.Cities.FirstOrDefault(x => x.Id == cityId);
                textBoxName.Text = city.Name;
            }

            this.Text = cityId.HasValue ? "Редактирование города" : "Добавление города";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxName.Text))
                return;

            if (!cityId.HasValue && context.Cities.Any(x => x.Name == textBoxName.Text))
            {
                MessageBox.Show("Город с таким названием уже добавлен", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                if (!cityId.HasValue)
                {
                    string[] separator = new string[] { "\r\n" };
                    string[] splits = textBoxName.Text.Split(separator, StringSplitOptions.None);

                    foreach (string split in splits)
                    {
                        if (!context.Cities.Any(x => x.Name == split)) context.Cities.Add(new City(split));
                    }
                }
                else
                    city.Name = textBoxName.Text;
                
                context.SaveChanges();

                Singleton.Main.InitCities();

                this.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
