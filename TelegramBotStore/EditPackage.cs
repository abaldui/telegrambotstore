﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;
using Escorp.Atom.Net;

namespace TelegramBotStore
{
    public partial class EditPackage : Form
    {
        private readonly ILog log = LogManager.GetLogger("TelegramBotSushi.EditPackage.cs");

        private BotContext context;

        private Product product;
        private Package package;

        private long cityId;
        private long regionId;
        private long productId;
        private long? packageId;

        public EditPackage(long _cityId, long _regionId, long _productId, long? _packageId)
        {
            InitializeComponent();

            cityId = _cityId;
            regionId = _regionId;
            productId = _productId;
            packageId = _packageId;
        }

        private void EditPackage_Load(object sender, EventArgs e)
        {
            context = BotContext.Create();

            City city = context.Cities.FirstOrDefault(x => x.Id == cityId);
            Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == regionId);
            product = region.Products.FirstOrDefault(x => x.Id == productId);

            if (packageId.HasValue)
            {
                package = product.Packages.FirstOrDefault(x => x.Id == packageId);
                textBoxName.Text = package.Name;
                textBoxPrice.Text = package.Price.ToString();
            }

            this.Text = packageId.HasValue ? "Редактирование фасовки отправления" : "Добавление фасовки отправления";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxName.Text) || String.IsNullOrEmpty(textBoxPrice.Text))
                return;

            if (!packageId.HasValue && product.Packages.Any(x => x.Name == textBoxName.Text))
            {
                MessageBox.Show("Фасовка с таким название уже добавлена", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
             decimal price = 0;
            decimal.TryParse(textBoxPrice.Text, out  price);

            if (price == 0)
            {
                MessageBox.Show("Введите цену", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                if (!packageId.HasValue)
                    product.Packages.Add(new Package(textBoxName.Text, price));
                else
                {
                    package.Name = textBoxName.Text;
                    package.Price = price;
                }

                context.SaveChanges();

                Singleton.Main.InitPackages();

                this.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
