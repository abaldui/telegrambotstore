﻿using System.Collections.Generic;

namespace TelegramBotStore.Domain
{
    public class City
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<Region> Regions { get; set; }

        public static long GlobalId;

        public City(string name)
        {
            Id = ++GlobalId;
            Name = name;
            Regions = new List<Region>();
        }

        public City() : this(null) { }
    }
}