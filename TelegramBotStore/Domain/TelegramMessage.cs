﻿using TeleSharp.TL;

namespace TelegramBotStore.Domain
{
    public class TelegramMessage
    {
        public TLMessage Message { get; set; }
        public TLUser User { get; set; }

        public TelegramMessage(TLMessage message, TLUser user)
        {
            Message = message;
            User = user;
        }
    }
}
