﻿using System;

namespace TelegramBotStore.Domain
{
    public class Order
    {
        public enum Statuses
        {
            InProcess,
            WaitingForPay,
            Payed,
            PaidNotEnough,
            Canceled,
            TimeOut
        }

        public long Id { get; set; }
        public Statuses Status { get; set; }
        public DateTime Din { get; set; }
        public DateTime? Date { get; set; }
        public decimal Payed { get; set; }
        public int NotificationNumber;

        public string City { get; set; }
        public string Region { get; set; }
        public string Product { get; set; }
        public string PackageName { get; set; }
        public decimal PackagePrice { get; set; }
        public string Address { get; set; }
        public string Comment { get; set; }
        public string QiwiNumber { get; set; }
        public bool IsDone { get; set; }

        public string PackageFull => $"{PackageName} за {Math.Round(PackagePrice, 0)} руб.";

        public static long GlobalId;

        public Order()
        {
            Id = ++GlobalId;
            Din = DateTime.Now;
        }
    }
}