﻿using System;

namespace TelegramBotStore.Domain
{
    public class Review
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public string From { get; set; }
        public DateTime Din { get; set; }

        public static long GlobalId;

        public Review(string text, string from)
        {
            Id = ++GlobalId;
            Text = text;
            From = from;
        }

        public Review(string text) : this(text, null) { }

        public Review() : this(null) { }
    }
}