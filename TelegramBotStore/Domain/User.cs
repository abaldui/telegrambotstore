﻿using System;
using System.Collections.Generic;

namespace TelegramBotStore.Domain
{
    public class User
    {
        public enum PayType
        {
            QIWI,
            WEX,
            Other,
            BlockChain,
            Card
        }

        public enum Actions
        {
            None,
            New,
            InPrice,
            InMenu,
            WaitingRegion,
            WaitingProduct,
            WaitingPackage,
            WaitingPackageNew,
            WaitingPaymentMethod,
            WaitingReview,
        }

        public long Id { get; set; }
        public int ChatId { get; set; }
        public long AccessHash { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Actions Action { get; set; }
        public DateTime Din { get; set; }
        public int? LastMessage { get; set; }
        public bool IsBan { get; set; }
        public string IsBantext { get { return IsBan ? "Да" : "Нет"; } }

        public PayType payType { get; set; } = PayType.Other;

        public List<Order> Orders { get; set; }

        public Order CurOrder { get; set; }

        public static long GlobalId;

        public User()
        {
            Id = ++GlobalId;
            Orders = new List<Order>();
        }
    }
}