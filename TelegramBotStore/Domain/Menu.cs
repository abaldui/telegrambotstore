﻿namespace TelegramBotStore.Domain
{
    public class Menu
    {
        public long Id { get; set; }
        public string Sign { get; set; }
        public string Name { get; set; }
        public string Page { get; set; }

        public static long GlobalId;

        public Menu(string sign, string name, string page)
        {
            Id = ++GlobalId;
            Sign = sign;
            Name = name;
            Page = page;
        }

        public Menu() : this(null, null, null) { }
    }
}