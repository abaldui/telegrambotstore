﻿using System.Collections.Generic;

namespace TelegramBotStore.Domain
{
    public class Package
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public List<Address> Addresses { get; set; }

        public static long GlobalId;

        public Package(string name, decimal price)
        {
            Id = ++GlobalId;
            Name = name;
            Price = price;
            Addresses = new List<Address>();
        }

        public Package(string name) : this(name, default(decimal)) { }

        public Package(decimal price) : this(null, price) { }

        public Package() : this(null) { }
    }
}