﻿using System.Collections.Generic;

namespace TelegramBotStore.Domain
{
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Package> Packages { get; set; }

        public static long GlobalId;

        public Product(string name, string description)
        {
            Id = ++GlobalId;
            Name = name;
            Description = description;
            Packages = new List<Package>();
        }

        public Product(string name) : this(name, null) { }

        public Product() : this(null) { }
    }
}