﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBotStore.Domain
{
    public class ProductRefill
    {
        public string City { get; set; }
        public string Region { get; set; }
        public string Product { get; set; }
        public string Package { get; set; }
        public decimal Price { get; set; }
        public string Code { get; set; }
    }
}
