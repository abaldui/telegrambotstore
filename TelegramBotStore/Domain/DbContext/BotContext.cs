﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TelegramBotStore.Domain.DbContext
{
    public enum IndexType
    {
        City,
        Region,
        Product,
        Package,
        Address,
        Review,
        User,
        Order,
        Menu,
    }

    public class BotContext : IDisposable
    {
        private const string DatabaseDir = "database/";
        private const string ContextJsonFile = DatabaseDir + "context.json";
        private const string ReviewsJsonFile = DatabaseDir + "reviews.json";

        [JsonProperty]
        public List<User> Users { get; set; }

        [JsonProperty]
        public List<City> Cities { get; set; }

        [JsonProperty]
        public List<Order> Orders { get; set; }

        [JsonIgnore]
        public List<Review> Reviews { get; set; }

        [JsonProperty]
        public List<Menu> Menu { get; set; }

        [JsonProperty]
        public Dictionary<IndexType, long> Indexes { get; set; }

        public BotContext()
        {
            Users = new List<User>();
            Cities = new List<City>();
            Orders = new List<Order>();
            Reviews = new List<Review>();
            Menu = new List<Menu>();

            Indexes = new Dictionary<IndexType, long>
            {
                [IndexType.City] = 0,
                [IndexType.Region] = 0,
                [IndexType.Product] = 0,
                [IndexType.Package] = 0,
                [IndexType.Address] = 0,
                [IndexType.Review] = 0,
                [IndexType.User] = 0,
                [IndexType.Order] = 0,
                [IndexType.Menu] = 0,
            };
        }

        internal static BotContext Create()
        {
            BotContext context = null;

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            string json = null;

            if (!Directory.Exists(DatabaseDir)) Directory.CreateDirectory(DatabaseDir);

            if (!File.Exists(ContextJsonFile))
            {
                context = new BotContext();

                serializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                json = JsonConvert.SerializeObject(context, serializerSettings);
                File.WriteAllText(ContextJsonFile, json);
            }

            if (!File.Exists(ReviewsJsonFile))
            {
                json = JsonConvert.SerializeObject(context.Reviews, serializerSettings);
                File.WriteAllText(ReviewsJsonFile, json);
            }
            
            if (File.Exists(ContextJsonFile)) context = JsonConvert.DeserializeObject<BotContext>(File.ReadAllText(ContextJsonFile));
            if (File.Exists(ReviewsJsonFile)) context.Reviews = JsonConvert.DeserializeObject<List<Review>>(File.ReadAllText(ReviewsJsonFile));

            return context;
        }

        internal void SaveChanges()
        {
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            Indexes[IndexType.City] = City.GlobalId;
            Indexes[IndexType.Region] = Region.GlobalId;
            Indexes[IndexType.Product] = Product.GlobalId;
            Indexes[IndexType.Package] = Package.GlobalId;
            Indexes[IndexType.Address] = Address.GlobalId;
            Indexes[IndexType.Review] = Review.GlobalId;
            Indexes[IndexType.User] = User.GlobalId;
            Indexes[IndexType.Order] = Order.GlobalId;
            Indexes[IndexType.Menu] = Domain.Menu.GlobalId;

            string json = JsonConvert.SerializeObject(this, serializerSettings);
            File.WriteAllText(ContextJsonFile, json);

            json = JsonConvert.SerializeObject(Reviews, serializerSettings);
            File.WriteAllText(ReviewsJsonFile, json);
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~BotContext() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}