﻿namespace TelegramBotStore.Domain
{
    public class Address
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public static long GlobalId;

        public Address(string name)
        {
            Id = ++GlobalId;
            Name = name;
        }

        public Address() : this(null) { }
    }
}