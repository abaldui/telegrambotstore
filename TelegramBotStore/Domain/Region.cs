﻿using System.Collections.Generic;

namespace TelegramBotStore.Domain
{
    public class Region
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<Product> Products { get; set; }

        public static long GlobalId;

        public Region(string name)
        {
            Id = ++GlobalId;
            Name = name;
            Products = new List<Product>();
        }

        public Region() : this(null) { }
    }
}