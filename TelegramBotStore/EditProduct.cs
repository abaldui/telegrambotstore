﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;

namespace TelegramBotStore
{
    public partial class EditProduct : Form
    {
        private readonly ILog log = LogManager.GetLogger("TelegramBotSushi.EditProduct.cs");

        private BotContext context;
   
        private Domain.Region region;
        private Product product;

        private long cityId;
        private long regionId;
        private long? productId;

        public EditProduct(long _cityId, long _regionId, long? _productId)
        {
            InitializeComponent();

            cityId = _cityId;
            regionId = _regionId;
            productId = _productId;
        }

        private void EditProduct_Load(object sender, EventArgs e)
        {
            context = BotContext.Create();

            City city = context.Cities.FirstOrDefault(x => x.Id == cityId);
            region = city.Regions.FirstOrDefault(x => x.Id == regionId);

            if (productId.HasValue)
            {
                product = region.Products.FirstOrDefault(x => x.Id == productId);
                textBoxName.Text = product.Name;
            }

            this.Text = productId.HasValue ? "Редактирование товара" : "Добавление товара";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxName.Text))
                return;

            if (!productId.HasValue && region.Products.Any(x => x.Name == textBoxName.Text))
            {
                MessageBox.Show("Товар с таким название уже добавлен", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                if (!productId.HasValue)
                {
                    string[] separator = new string[] { "\r\n" };
                    string[] splits = textBoxName.Text.Split(separator, StringSplitOptions.None);

                    foreach (string split in splits)
                    {
                        if (!region.Products.Any(x => x.Name == split)) region.Products.Add(new Product(split));
                    }
                }
                else
                    product.Name = textBoxName.Text;
                
                context.SaveChanges();

                Singleton.Main.InitProducts();

                this.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
