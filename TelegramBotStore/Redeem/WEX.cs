﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Escorp.Atom.Net.Http;
using Newtonsoft.Json.Linq;

namespace TelegramBotStore.Redeem
{
    public static class UnixTime
    {
        static DateTime unixEpoch;
        static UnixTime()
        {
            unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        public static UInt32 Now { get { return GetFromDateTime(DateTime.UtcNow); } }
        public static UInt32 GetFromDateTime(DateTime d) { return (UInt32)(d - unixEpoch).TotalSeconds; }
        public static DateTime ConvertToDateTime(UInt32 unixtime) { return unixEpoch.AddSeconds(unixtime); }
    }
    public class WEX
    {
        private string coupon { get; set; }
        private decimal amount { get; set; }
        private HMACSHA512 hashMaker;
        private string key = Properties.Settings.Default.API;
        private string secret = Properties.Settings.Default.SECRET;

        public WEX(string coupon, decimal amount)
        {
            this.coupon = coupon;
            this.amount = amount;
            hashMaker = new HMACSHA512(Encoding.ASCII.GetBytes(secret));
        }
        static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }
        public bool Activate()
        {
            using (var http = new HttpRequest())
            {
                var dataStr = $"coupon={coupon}&nonce={UnixTime.Now}&method=RedeemCoupon";

                var data = Encoding.ASCII.GetBytes(dataStr);
                var sign = ByteArrayToString(hashMaker.ComputeHash(data)).ToLower();
                http.AddHeader("Key", key);
                http.AddHeader("Sign", sign);
                var respStr = http.Post("https://wex.nz/tapi", dataStr, "application/x-www-form-urlencoded").ToString();
                var obj = JObject.Parse(respStr);
                if (respStr.Contains("\"success\":1,") && respStr.Contains("couponAmount"))
                    if ((Convert.ToDecimal(obj["return"]["couponAmount"].ToString().Replace(".", ",")) >= amount) && (obj["return"]["couponCurrency"].ToString() == "RUR"))
                        return true;
            }
            return false;
        }
    }
}
