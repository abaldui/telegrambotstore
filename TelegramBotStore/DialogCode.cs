﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelegramBotStore
{
    public partial class DialogCode : Form
    {
        public string Code { get { return textBoxCode.Text; } }

        public DialogCode()
        {
            InitializeComponent();
        }

        private void DialogCode_Load(object sender, EventArgs e)
        {
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;
        }
    }
}
