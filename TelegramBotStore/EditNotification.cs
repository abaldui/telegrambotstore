﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelegramBotStore
{
    public partial class EditNotification : Form
    {
        private List<string> _names;

        public EditNotification()
        {
            InitializeComponent();
        }

        private void EditNotification_Load(object sender, EventArgs e)
        {
            _names = Emoji.GetNames(typeof(Emoji));
            AutoCompleteStringCollection autoCompleteStringCollection = new AutoCompleteStringCollection();
            _names.ForEach(x =>
            {
                autoCompleteStringCollection.Add(x);
            });

            txtSearch.AutoCompleteCustomSource = autoCompleteStringCollection;
            txtSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;

            List<string> values = Emoji.GetValues<string>(typeof(Emoji));
            values.ForEach(x =>
            {
                listBoxEmoji.Items.Add(x);
            });
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (listBoxEmoji.SelectedItem != null)
                txtBody.Text = txtBody.Text.Insert(txtBody.SelectionStart, listBoxEmoji.SelectedItem.ToString());
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string text = String.Empty;

            if (!String.IsNullOrEmpty(txtBody.Text))
                Singleton.Main.SendMessage(txtBody.Text);

            this.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string text = txtSearch.Text;

            int index = _names.IndexOf(text);

            if (index != -1)
                listBoxEmoji.SelectedIndex = index;
        }
    }
}
