﻿using log4net;
using System;
using System.Linq;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;

namespace TelegramBotStore
{
    public partial class EditAll : Form
    {
        private readonly ILog log = LogManager.GetLogger("TelegramBotStore.EditAll.cs");
        private BotContext context;

        private Region region;
        private Product product;
        private Package package;
        private long cityId;
        private long regionId;
        private long? prodALL;

        public EditAll(long _cityId, long _regionId, long? _prodALL)
        {
            InitializeComponent();

            cityId = _cityId;
            regionId = _regionId;
            prodALL = _prodALL;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text))
                return;

            if (allregion.Checked)
            {
                try
                {
                    BotContext context = BotContext.Create();
                    string[] separator = new string[] { "\r\n" };
                    string[] splits = textBox1.Text.Split(separator, StringSplitOptions.None);
                    City city = context.Cities.FirstOrDefault(x => x.Id == cityId);
                    Product prod = null;
                    Package pakk = null;
                    if (deleteall.Checked)
                    {
                        bool ok = true;
                        do
                        {
                            foreach (Domain.Region reg in city.Regions)
                            {
                                reg.Products.Clear();
                            }
                        }
                        while (!ok);
                            context.SaveChanges();
                    }
                   
                    foreach (string split in splits)
                    {
                        string[] splits1 = split.Split(new Char[] { '-' }, StringSplitOptions.None);
                        decimal price = 0;

                        if(splits1.Length < 3)
                            continue;

                        decimal.TryParse(splits1[2], out price);
                        
                        foreach ( Domain.Region reg in  city.Regions)
                        {
                             prod = reg.Products.Find(x => x.Name == splits1[0]);
                            if (prod == null)
                            {
                                reg.Products.Add(new Product(splits1[0]));
                                product = reg.Products.Last();
                                product.Packages.Add(new Package(splits1[1], price));
                                pakk = product.Packages.Last();
                                pakk.Addresses.Add(new Address(Singleton.Main.getComment(6)));
                                pakk = null;
                                prod = null;

                            }
                            else
                            {
                                pakk = prod.Packages.Find(x => x.Name == splits1[1]);
                                if (pakk == null)
                                {
                                    prod.Packages.Add(new Package(splits1[1], price));
                                    pakk = prod.Packages.Last();
                                    pakk.Addresses.Add(new Address(Singleton.Main.getComment(6)));
                                    pakk = null;
                                }
                            }
                        } 
                    }
                    context.SaveChanges(); 

                    //Singleton.Main.InitProducts();
                    //Singleton.Main.InitPackages();
                    this.Close();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
            else
            {
                try
                {
                    BotContext context = BotContext.Create();
                    string[] separator = new string[] { "\r\n" };
                    string[] splits = textBox1.Text.Split(separator, StringSplitOptions.None);

                    City city = context.Cities.FirstOrDefault(x => x.Id == cityId);
                    region = city.Regions.FirstOrDefault(x => x.Id == regionId);
                    Product prod = null;
                    Package pakk = null;
                    

                    foreach (string split in splits)
                    {
                        string[] splits1 = split.Split(new Char[] { '-' }, StringSplitOptions.None);
                        decimal price = 0;
                        decimal.TryParse(splits1[2], out price);

                        prod = region.Products.Find(x => x.Name == splits1[0]);
                        if (prod == null)
                        {
                            region.Products.Add(new Product(splits1[0]));
                            product = region.Products.Last();
                            product.Packages.Add(new Package(splits1[1], price));
                            pakk = product.Packages.Last();
                            pakk.Addresses.Add(new Address(Singleton.Main.getComment(6)));
                            pakk = null;
                            prod = null;

                        }
                        else
                        {
                            pakk = prod.Packages.Find(x => x.Name == splits1[1]);
                            if (pakk == null)
                            {
                                prod.Packages.Add(new Package(splits1[1], price));
                                pakk = prod.Packages.Last();
                                pakk.Addresses.Add(new Address(Singleton.Main.getComment(6)));
                                pakk = null;
                            }
                        }
                    }
                    context.SaveChanges();
                    Singleton.Main.InitProducts();
                    Singleton.Main.InitPackages();
                    this.Close();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
        }


        private void EditAll_Load(object sender, EventArgs e)
        {
            context = BotContext.Create();

            City city = context.Cities.FirstOrDefault(x => x.Id == cityId);
            region = city.Regions.FirstOrDefault(x => x.Id == regionId);
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}