﻿namespace TelegramBotStore
{
    partial class EditAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnNo = new System.Windows.Forms.Button();
            this.allregion = new System.Windows.Forms.CheckBox();
            this.deleteall = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(3, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(996, 537);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Товар/Фасовка/Цена";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 18);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(990, 516);
            this.textBox1.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.Image = global::TelegramBotStore.Properties.Resources.confirm;
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOK.Location = new System.Drawing.Point(518, 544);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(117, 34);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Сохранить";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnNo
            // 
            this.btnNo.Image = global::TelegramBotStore.Properties.Resources.cancel;
            this.btnNo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNo.Location = new System.Drawing.Point(397, 544);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(115, 34);
            this.btnNo.TabIndex = 2;
            this.btnNo.Text = "Отмена";
            this.btnNo.UseVisualStyleBackColor = true;
            this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
            // 
            // allregion
            // 
            this.allregion.AutoSize = true;
            this.allregion.Location = new System.Drawing.Point(6, 544);
            this.allregion.Name = "allregion";
            this.allregion.Size = new System.Drawing.Size(333, 21);
            this.allregion.TabIndex = 3;
            this.allregion.Text = "Заполнить во всех районах выбраного города";
            this.allregion.UseVisualStyleBackColor = true;
            // 
            // deleteall
            // 
            this.deleteall.AutoSize = true;
            this.deleteall.Location = new System.Drawing.Point(6, 562);
            this.deleteall.Name = "deleteall";
            this.deleteall.Size = new System.Drawing.Size(273, 21);
            this.deleteall.TabIndex = 4;
            this.deleteall.Text = "Удалить товары перед заполнением";
            this.deleteall.UseVisualStyleBackColor = true;
            // 
            // EditAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 584);
            this.Controls.Add(this.deleteall);
            this.Controls.Add(this.allregion);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EditAll";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EditAll";
            this.Load += new System.EventHandler(this.EditAll_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnNo;
        private System.Windows.Forms.CheckBox allregion;
        private System.Windows.Forms.CheckBox deleteall;
    }
}