﻿using System;
using System.Windows.Forms;

namespace TelegramBotStore
{
    public partial class FormListQiwiNumbers : Form
    {

        public FormListQiwiNumbers()
        {
            InitializeComponent();
            this.Text = "List of qiwi numbers";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.QiwiNumbersStr = textBoxNumbers.Text;
            this.Close();
        }

        private void FormListQiwiNumbers_Load(object sender, EventArgs e)
        {
            textBoxNumbers.Text = Properties.Settings.Default.QiwiNumbersStr;
        }
    }
}
