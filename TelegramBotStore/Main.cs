﻿using Escorp.Atom.Net.Proxy;
using log4net;
using QiwiApi;
using QiwiApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;
using TelegramBotStore.Redeem;
using TeleSharp.TL;
using TeleSharp.TL.Messages;
using TeleSharp.TL.Updates;
using TLSharp.Core;
using TLSharp.Core.Utils;
using Info.Blockchain.API.ExchangeRates;
using System.Xml.Linq;
using System.Globalization;
using System.Threading;

namespace TelegramBotStore
{
    public enum Currency
    {
        RUB,
        KZT
    }

    public partial class Main : Form
    {
        #region ===properties===

        private readonly ILog log = LogManager.GetLogger("TelegramBotStore.Main.cs_1.1.5");

        private const string UNKNOWN_CODE = "Неправильный выбор, попробуйте еще раз. \n" +
                                            "Для выбора варианта отправьте ЧИСЛО или СИМВОЛ слева от нужного варианта, например 1 или #";

        private static Currency CURRENCY_CURRENT;
        private static string CURRENCY_NAME;
        private bool IsWexAvailable = false;
        private bool IsBlockChainAvailable = false;
        private bool IsCardAvailable = false;

        private TelegramClient _client;
        private Qiwi qiwi;

        //BlockChain
        private string _blockChainAdderess;
        private string _blockChainApiKey;
        private string _blockChainXPub;
        private decimal _discountBTC;

        private static Random rand = new Random();
        private volatile bool isCanceled;
        private int PTS;
        private string PATH = Directory.GetCurrentDirectory() + @"\Images\";

        private long? selectedUserId;
        private long? selectedCityId;
        private long? selectedRegionId;
        private long? selectedProductId;
        private long? selectedPackageId;

        #endregion

        public Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();

            Application.ApplicationExit += Application_ApplicationExit;
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            UnCheckQiwiCurrentNumber();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = "TelegramBotStore 1.1.5";
                Singleton.Main = this;

                initUsers();
                initOrders();
                InitCities();
                InitRegions();
                InitProducts();
                InitPackages();
                InitAddresses();
                initRevies();

                initQiwi();
                ckeckQiwiBalance();
                initBlockChain();

                Task.Run(() => isAuthorized());
            }
            catch (Exception ex)
            {
                log.Error(ex);
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonAuthorize_Click(object sender, EventArgs e)
        {
            Task.Run(() => authorize());
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (_client != null && !_client.IsUserAuthorized())
            {
                MessageBox.Show("Сначала нужно авторизироваться", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            BotContext context = BotContext.Create();

            if (!context.Cities.Any())
            {
                MessageBox.Show("Сначала нужно добавить город", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!context.Cities.Where(x => x.Regions.Any()).Any())
            {
                MessageBox.Show("Сначала нужно добавить район", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!context.Cities.Where(x => x.Regions.Any(r => r.Products.Any())).Any())
            {
                MessageBox.Show("Сначала нужно добавить товар", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!context.Cities.Where(x => x.Regions.Any(r => r.Products.Any(y => y.Packages.Any()))).Any())
            {
                MessageBox.Show("Сначала нужно добавить фасовку", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!context.Cities.Where(x => x.Regions.Any(r => r.Products.Any(y => y.Packages.Any(a => a.Addresses.Any())))).Any())
            {
                MessageBox.Show("Сначала нужно добавить куплено", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CURRENCY_CURRENT = (Currency)Properties.Settings.Default.Currency;
            switch (CURRENCY_CURRENT)
            {
                case Currency.RUB:
                    CURRENCY_NAME = "руб";
                    break;
                case Currency.KZT:
                    CURRENCY_NAME = "тенге";
                    break;
                default:
                    MessageBox.Show("Ошибка выбора валют!");
                    return;
            }

            IsWexAvailable = Properties.Settings.Default.IsWexAvailable;
            IsBlockChainAvailable = Properties.Settings.Default.IsBlockChainAvailable;
            IsCardAvailable = Properties.Settings.Default.IsCardAvailable;
            PATH_QIWI_FILE = Properties.Settings.Default.PATH_QIWI_FILE;

            if (PATH_QIWI_FILE == null || string.IsNullOrEmpty(PATH_QIWI_FILE))
            {
                MessageBox.Show("Выберите файл киви!");
                return;
            }

            initBlockChain();

            isCanceled = false;

            Task.Run(() => doWork());
            Task.Run(() => UpdateReviewsDate());

            timerTimeToWait.Start();

            disableControls();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            isCanceled = true;
            UnCheckQiwiCurrentNumber();

            timerTimeToWait.Stop();
        }

        private void товарУпаковкаценаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedRegionId.HasValue)
            {
                EditAll dialog = new EditAll(selectedCityId.Value, selectedRegionId.Value, 122);
                dialog.ShowDialog();
                InitProducts();
                InitPackages();
            }
            else
                MessageBox.Show("Выберите город, Район ", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void добавитьКоментарийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Comment dialog = new Comment();
            dialog.ShowDialog();
            initRevies();
        }

        private void buttonRefreshComment_Click(object sender, EventArgs e)
        {
            initRevies();
        }

        private void dataGridViewUsers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            DataGridViewImageColumn column = senderGrid.Columns[e.ColumnIndex] as DataGridViewImageColumn;

            if (e.RowIndex != -1)
            {
                try
                {
                    selectedUserId = long.Parse(senderGrid["Id", e.RowIndex].Value.ToString());

                    BotContext context = BotContext.Create();
                    List<User> list = context.Users.ToList();
                    User user = list.FirstOrDefault(x => x.Id == selectedUserId.Value);

                    if (column != null)
                    {
                        if (column.Name == "ban")
                        {
                            user.IsBan = !user.IsBan;
                            context.SaveChanges();

                            initUsers(context.Users.ToList());
                        }
                    }

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }

            initOrders();
            initRevies();
        }

        private void dataGridViewOrders_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && e.Button == MouseButtons.Right && dataGridViewOrders.SelectedCells.Count == 1)
            {
                DataGridViewCell c = (sender as DataGridView)[e.ColumnIndex, e.RowIndex];

                if (c.ColumnIndex == 1 && c.Value.ToString().Equals(Order.Statuses.WaitingForPay.ToString()))
                {
                    contextMenuOrder.Show(dataGridViewOrders, dataGridViewOrders.PointToClient(Cursor.Position));
                }
            }
        }

        private void ToolStripMenuItemSetStatusOrder_Click(object sender, EventArgs e)
        {
            if (dataGridViewOrders.SelectedCells.Count == 1)
            {
                long id = Int64.Parse(dataGridViewOrders.Rows[dataGridViewOrders.SelectedCells[0].RowIndex].Cells["Id"].Value.ToString());

                using (BotContext context = BotContext.Create())
                {
                    User usr = context.Users.FirstOrDefault(m => m.Orders.Any(x => x.Id == id));

                    if (usr.CurOrder != null)
                    {
                        usr.CurOrder.Status = Order.Statuses.Payed;
                        usr.CurOrder.IsDone = true;
                        cancelPackage(context, usr.CurOrder);
                    }
                    usr.payType = User.PayType.Other;
                    usr.Action = User.Actions.None;

                    foreach (Order o in usr.Orders)
                    {
                        if (o.Id == usr.CurOrder.Id)
                        {
                            o.Address = usr.CurOrder.Address;
                            o.City = usr.CurOrder.City;
                            o.Comment = usr.CurOrder.Comment;
                            o.Date = usr.CurOrder.Date;
                            o.Din = usr.CurOrder.Din;
                            o.IsDone = usr.CurOrder.IsDone;
                            o.PackageName = usr.CurOrder.PackageName;
                            o.PackagePrice = usr.CurOrder.PackagePrice;
                            o.Payed = usr.CurOrder.Payed;
                            o.Product = usr.CurOrder.Product;
                            o.Region = usr.CurOrder.Region;
                            o.Status = usr.CurOrder.Status;
                        }
                    }
                    context.SaveChanges();
                }
            }
        }

        private void dataGridViewCities_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                DataGridViewImageColumn column = senderGrid.Columns[e.ColumnIndex] as DataGridViewImageColumn;

                selectedCityId = long.Parse(senderGrid["Id", e.RowIndex].Value.ToString());

                if (column != null)
                {
                    if (column.Name == "edit")
                    {
                        EditCity dialog = new EditCity(selectedCityId);
                        dialog.ShowDialog();
                    }
                    else if (column.Name == "remove")
                    {
                        BotContext context = BotContext.Create();

                        City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId);
                        context.Cities.Remove(city);

                        context.SaveChanges();

                        selectedCityId = null;
                        selectedRegionId = null;
                        selectedProductId = null;
                        selectedPackageId = null;

                        InitCities();
                        InitRegions();
                        InitProducts();
                        InitPackages();
                        InitAddresses();
                    }
                }
                else
                {
                    selectedRegionId = null;
                    selectedProductId = null;
                    selectedPackageId = null;

                    InitRegions();
                    InitProducts();
                    InitPackages();
                    InitAddresses();
                }
            }
        }

        private void dataGridViewRegions_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                DataGridViewImageColumn column = senderGrid.Columns[e.ColumnIndex] as DataGridViewImageColumn;

                selectedRegionId = long.Parse(senderGrid["Id", e.RowIndex].Value.ToString());

                if (column != null)
                {
                    if (column.Name == "edit")
                    {
                        EditRegion dialog = new EditRegion(selectedCityId.Value, selectedRegionId);
                        dialog.ShowDialog();
                    }
                    else if (column.Name == "remove")
                    {
                        BotContext context = BotContext.Create();

                        City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                        Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == selectedRegionId);
                        city.Regions.Remove(region);

                        context.SaveChanges();

                        selectedRegionId = null;
                        selectedProductId = null;
                        selectedPackageId = null;

                        InitRegions();
                        InitProducts();
                        InitPackages();
                        InitAddresses();
                    }
                }
                else
                {
                    selectedProductId = null;
                    selectedPackageId = null;

                    InitProducts();
                    InitPackages();
                    InitAddresses();
                }
            }
        }

        private void dataGridViewProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                DataGridViewImageColumn column = senderGrid.Columns[e.ColumnIndex] as DataGridViewImageColumn;

                selectedProductId = long.Parse(senderGrid["Id", e.RowIndex].Value.ToString());

                if (column != null)
                {
                    if (column.Name == "edit")
                    {
                        EditProduct dialog = new EditProduct(selectedCityId.Value, selectedRegionId.Value, selectedProductId.Value);
                        dialog.ShowDialog();
                    }
                    else if (column.Name == "remove")
                    {
                        BotContext context = BotContext.Create();

                        City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                        Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == selectedRegionId.Value);
                        Product product = region.Products.FirstOrDefault(x => x.Id == selectedProductId.Value);
                        region.Products.Remove(product);

                        context.SaveChanges();

                        selectedProductId = null;
                        selectedPackageId = null;

                        InitProducts();
                        InitPackages();
                        InitAddresses();
                    }
                }
                else
                {
                    selectedPackageId = null;

                    InitPackages();
                    InitAddresses();
                }
            }
        }

        private void dataGridViewPackaging_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                DataGridViewImageColumn column = senderGrid.Columns[e.ColumnIndex] as DataGridViewImageColumn;

                selectedPackageId = long.Parse(senderGrid["Id", e.RowIndex].Value.ToString());

                if (column != null)
                {
                    if (column.Name == "edit")
                    {
                        EditPackage dialog = new EditPackage(selectedCityId.Value, selectedRegionId.Value, selectedProductId.Value, selectedPackageId);
                        dialog.ShowDialog();
                    }
                    else if (column.Name == "remove")
                    {
                        BotContext context = BotContext.Create();

                        City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                        Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == selectedRegionId.Value);
                        Product product = region.Products.FirstOrDefault(x => x.Id == selectedProductId.Value);
                        Package package = product.Packages.FirstOrDefault(x => x.Id == selectedPackageId);

                        product.Packages.Remove(package);

                        context.SaveChanges();

                        selectedPackageId = null;

                        InitPackages();
                    }
                }
                else
                {
                    InitAddresses();
                }
            }
        }

        private void dataGridViewAddress_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                DataGridViewImageColumn column = senderGrid.Columns[e.ColumnIndex] as DataGridViewImageColumn;

                long _addressId = long.Parse(senderGrid["Id", e.RowIndex].Value.ToString());

                if (column != null)
                {
                    if (column.Name == "edit")
                    {
                        EditAddress dialog = new EditAddress(selectedCityId.Value, selectedRegionId.Value, selectedProductId.Value, selectedPackageId.Value, _addressId);
                        dialog.ShowDialog();
                    }
                    else if (column.Name == "remove")
                    {
                        BotContext context = BotContext.Create();

                        City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                        Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == selectedRegionId.Value);
                        Product product = region.Products.FirstOrDefault(x => x.Id == selectedProductId.Value);
                        Package package = product.Packages.FirstOrDefault(x => x.Id == selectedPackageId);
                        Domain.Address address = package.Addresses.FirstOrDefault(x => x.Id == _addressId);

                        package.Addresses.Remove(address);

                        context.SaveChanges();

                        InitAddresses();
                    }
                }
            }
        }

        private void dataGridViewReview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                DataGridViewImageColumn column = senderGrid.Columns[e.ColumnIndex] as DataGridViewImageColumn;

                long selectedReviewId = long.Parse(senderGrid["Id", e.RowIndex].Value.ToString());

                if (column != null)
                {
                    BotContext context = BotContext.Create();

                    Review review = context.Reviews.FirstOrDefault(x => x.Id == selectedReviewId);
                    context.Reviews.Remove(review);

                    context.SaveChanges();

                    initRevies();
                }
            }
        }

        private void ToolStripMenuItemSettings_Click(object sender, EventArgs e)
        {
            EditSettings dialog = new EditSettings();
            dialog.Show();
        }

        private void ToolStripMenuItemAddCity_Click(object sender, EventArgs e)
        {
            EditCity dialog = new EditCity(null);
            dialog.ShowDialog();
        }

        private void ToolStripMenuItemAddRegiob_Click(object sender, EventArgs e)
        {
            if (selectedCityId.HasValue)
            {
                EditRegion dialog = new EditRegion(selectedCityId.Value, null);
                dialog.ShowDialog();
            }
            else
                MessageBox.Show("Выберите город", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ToolStripMenuItemAddProduct_Click(object sender, EventArgs e)
        {
            if (selectedRegionId.HasValue)
            {
                EditProduct dialog = new EditProduct(selectedCityId.Value, selectedRegionId.Value, null);
                dialog.ShowDialog();
            }
            else
                MessageBox.Show("Выберите город, район", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ToolStripMenuItemAddPackaging_Click(object sender, EventArgs e)
        {
            if (selectedProductId.HasValue)
            {
                EditPackage dialog = new EditPackage(selectedCityId.Value, selectedRegionId.Value, selectedProductId.Value, null);
                dialog.ShowDialog();
            }
            else
                MessageBox.Show("Выберите город, район, товар", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ToolStripMenuItemSending_Click(object sender, EventArgs e)
        {
            EditNotification dialog = new EditNotification();
            dialog.ShowDialog();
        }

        private void ToolStripMenuItemMenu_Click(object sender, EventArgs e)
        {
            EditMenu dialog = new EditMenu();
            dialog.ShowDialog();
        }

        private void адресToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedPackageId.HasValue)
            {
                EditAddress dialog = new EditAddress(selectedCityId.Value, selectedRegionId.Value, selectedProductId.Value, selectedPackageId.Value, null);
                dialog.ShowDialog();
            }
            else
                MessageBox.Show("Выберите город, район, товар, фасовку", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void timerTimeToWait_Tick(object sender, EventArgs e)
        {
            Task.Run(() => updateOrdersTimeOut());
        }

        #region ===private===

        private class TelegramMessageComparer : IEqualityComparer<TelegramMessage>
        {
            public bool Equals(TelegramMessage f1, TelegramMessage f2) => f1.Message.Id == f2.Message.Id && f1.Message.FromId == f2.Message.FromId && f1.User.Id == f2.User.Id;

            public int GetHashCode(TelegramMessage f) => f.GetHashCode();
        }

        private List<TelegramMessage> getNewMessagesAsync(TLState state)
        {
            List<TelegramMessage> result = new List<TelegramMessage>();

            int seq = state.Seq;
            var req = new TLRequestGetDifference() { Date = state.Date, Pts = PTS, Qts = state.Qts, Sequence = state.Seq };

            var diff = _client.SendRequestAsync<TLAbsDifference>(req).Result as TLDifference;

            if (diff != null)
            {
                PTS = diff.State.Pts;

                foreach (var message in diff.NewMessages.ToList().OfType<TLMessage>())
                {
                    if (message.FromId.HasValue)
                    {
                        TLUser _user = diff.Users.ToList()
                       .Where(x => x.GetType() == typeof(TLUser))
                       .Cast<TLUser>()
                       .FirstOrDefault(x => x.Id == message.FromId.Value);

                        if (!message.Out)
                        {
                            result.Add(new TelegramMessage(message, _user));
                        }
                        else
                            MarkMessageRead(_client, new TLInputPeerUser() { UserId = _user.Id, AccessHash = _user.AccessHash.Value }, message.Id);
                    }
                }
            }

            return result;
        }

        private async void doWork()
        {
            TLState state = null;

            if (isCanceled)
            {
                isCanceled = false;
                log.Error("doWork() = WHILE START THREAD 'doWork' paramether 'isCanceled' WAS TRUE.");
            }

            while (!isCanceled)
            {
                try
                {
                    if (state == null)
                    {
                        state = await _client.SendRequestAsync<TLState>(new TLRequestGetState());
                        PTS = state.Pts;
                    }

                    List<TelegramMessage> messages = getNewMessagesAsync(state);
                    foreach (TelegramMessage _message in messages)
                    {
                        using (BotContext context = BotContext.Create())
                        {
                            TLMessage message = _message.Message;

                            try
                            {
                                User user = getUser(context, _message.Message, _message.User);

                                Domain.Menu menu = null;

                                if (user.IsBan)
                                {
                                    //TODO бан
                                    //await showBan(context, user, menu);
                                    continue;
                                }

                                if (!message.Out)
                                {
                                    menu = context.Menu.FirstOrDefault(x => x.Sign == message.Message);

                                    if (menu != null)
                                    {
                                        await showMenuNew(context, user, menu);
                                    }
                                    else if (isSpam())
                                    {

                                    }
                                    else if (message.Message.Contains("/start") || user.Action == User.Actions.New || message.Message == "#")
                                    {
                                        await showMenu(context, user);
                                    }
                                    else if (message.Message == "+")
                                    {
                                        await showPriceList(context, user);
                                    }
                                    else if (message.Message == "111" && user.payType == User.PayType.QIWI && user.CurOrder.Status == Order.Statuses.WaitingForPay)
                                    {
                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                            "Идет проверка.");
                                        Thread.Sleep(rand.Next(5, 10) * 1000);

                                        await showOrders(context, user, message.FromId.Value);
                                    }
                                    else if (message.Message == "111" && user.payType == User.PayType.BlockChain && user.CurOrder.Status == Order.Statuses.WaitingForPay
                                        && IsBlockChainAvailable)
                                    {
                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                            "Идет проверка.");
                                        Thread.Sleep(rand.Next(5, 10) * 1000);

                                        var text = "Ожидается платеж, заказ обрабатывается.\n";
                                        text += getMenu(context, user);
                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
                                    }
                                    //else if (message.Message == "111" && user.payType == User.PayType.WEX && user.CurOrder.Status == Order.Statuses.WaitingForPay) 
                                    //{
                                    //    var text = "Напишите WEX-код в течении " + Properties.Settings.Default.TimeToWait + " часов\n";
                                    //    text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + "\n" +
                                    //            "Сумма: " + Math.Round(user.CurOrder.PackagePrice) + " рублей " + "\n" +
                                    //            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + "\n";
                                    //    text += "WEX: 123424\n";
                                    //    text += getMenu(context, user);
                                    //    await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
                                    //}
                                    else if (message.Message == "111" && user.payType == User.PayType.Card && user.CurOrder.Status == Order.Statuses.WaitingForPay)
                                    {
                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                            "Идет проверка.");
                                        Thread.Sleep(rand.Next(5, 10) * 1000);

                                        await showOrders(context, user, message.FromId.Value);
                                    }
                                    else if (message.Message != "%" && user.payType == User.PayType.WEX && user.CurOrder.Status == Order.Statuses.WaitingForPay
                                        && IsWexAvailable)
                                    {
                                        await showOrdersWEX(context, user, message.Message);
                                    }
                                    else if (message.Message == "111" && (user.payType == User.PayType.Other || user.CurOrder.Status != Order.Statuses.WaitingForPay))
                                    {
                                        string text = "У вас нет заказов ожидающих оплату";
                                        text += getMenu(context, user);
                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
                                    }
                                    else if (message.Message == "%")
                                    {
                                        if (user.CurOrder != null)
                                        {
                                            user.CurOrder.Status = Order.Statuses.Canceled;
                                            cancelPackage(context, user.CurOrder);
                                        }

                                        user.payType = User.PayType.Other;
                                        user.Action = User.Actions.None;
                                        await showMenu(context, user);
                                    }
                                    else if (message.Message == "777")
                                    {
                                        await showReview(context, user);
                                    }
                                    else if (message.Message == "999")
                                    {
                                        await showReviewAll(context, user);
                                    }
                                    else if (user.Action == User.Actions.WaitingReview)
                                    {
                                        string text = Emoji.No_Entry_Sign + " Отзыв не может быть пустым.";

                                        if (!String.IsNullOrEmpty(message.Message))
                                        {
                                            context.Reviews.Add(new Review() { Text = message.Message, Din = DateTime.Now, From = user.UserName });

                                            text = "Спасибо за отзыв.";

                                            user.Action = User.Actions.None;
                                        }

                                        text += getMenu(context, user);

                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
                                    }
                                    else if (message.Message.ToLower() == "проба" || message.Message.ToLower() == "пробник")
                                    {
                                        await sendPhoto(user, EditSettings.SPECIMEN_PHOTO, Properties.Settings.Default.Greeting);

                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                            getMenu(context, user));
                                    }
                                    else if (user.Action == User.Actions.None)
                                    {
                                        if (menu != null)
                                            await showMenuNew(context, user, menu);
                                        else
                                        {
                                            await showRegions(context, user, message, true);
                                        }
                                    }
                                    else if (user.Action == User.Actions.WaitingRegion)
                                    {
                                        await showRegionProductsCity(context, user, message);
                                    }
                                    else if (user.Action == User.Actions.WaitingProduct)
                                    {
                                        await showPackagesNew(context, user, message, menu);
                                    }
                                    else if (user.Action == User.Actions.WaitingPackage)
                                    {
                                        if (message.Message.Any(x => char.IsLetter(x)))
                                        {
                                            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                                   UNKNOWN_CODE);
                                        }
                                        else
                                            await showRegionsNEW(context, user, message);
                                    }
                                    else if (user.Action == User.Actions.WaitingPackageNew)
                                    {
                                        try
                                        {
                                            await showPaymentNew(context, user, message, menu);
                                        }
                                        catch (Exception er)
                                        {
                                            if (er is KeyNotFoundException)
                                            {
                                                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                                   "Нажмите #");
                                            }
                                            else
                                            {
                                                throw er;
                                            }
                                        }
                                    }
                                    else if (user.Action == User.Actions.WaitingPaymentMethod)
                                    {
                                        await showOrdered(context, user, message, menu);
                                    }
                                    else
                                    {
                                        await showUnknown(context, user);
                                    }
                                }
                                else
                                {
                                    await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, UNKNOWN_CODE);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex);
                            }
                            finally
                            {
                                MarkMessageRead(_client, new TLInputPeerUser() { UserId = _message.User.Id, AccessHash = _message.User.AccessHash.Value }, message.Id);

                                context?.SaveChanges();
                            }
                        }
                    }

                    //await Task.Delay(1000 + rand.Next(1, 500));
                    Thread.Sleep(1000 + rand.Next(1, 500));
                }
                catch (Exception ex)
                {
                    log.Error("doWork() = ", ex);

                    var err = ex as TLSharp.Core.Network.FloodException;
                    if (err != null)
                    {
                        updateStatus("Бан, время ожидания "
                            + err.TimeToWait + "сек. Начало " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), Color.Black);
                        Thread.Sleep(err.TimeToWait);
                    }

                    await isAuthorized();
                }
                finally
                {
                    if (isCanceled)
                        enableControls();
                }
            }
        }

        private async Task isAuthorized()
        {
            try
            {
                updateStatus(null, Color.Black);

                string phone = Properties.Settings.Default.Phone;
                int api_id = Properties.Settings.Default.Api_Id;
                string api_hash = Properties.Settings.Default.Api_Hash;

                if (String.IsNullOrEmpty(phone) || String.IsNullOrEmpty(api_hash))
                {
                    updateStatus("Не авторизирован", Color.Red);

                    return;
                }

                _client = new TelegramClient(api_id, api_hash,
                    handler: new TLSharp.Core.Network.TcpClientConnectionHandler(TcpHandler));

                await _client.ConnectAsync();

                if (!await _client.IsPhoneRegisteredAsync(phone)) throw new Exception("Номер не зарегистрирован");

                if (!_client.IsUserAuthorized())
                    /*{
                        _client.MakeAuthWithPasswordAsync();
                    }*/
                    updateStatus("Не авторизирован", Color.Red);
                else
                    updateStatus("Авторизирован", Color.Green);
            }
            catch (InvalidPhoneCodeException ex)
            {
                log.Error(ex);
                MessageBox.Show("CodeToAuthenticate is wrong in the app.config file, fill it with the code you just got now by SMS/Telegram", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                updateStatus("Ошибка: " + ex.Message, Color.Red);
            }
        }

        /*The problem solved with follow these steps and I hope this helps.

    Change phone number to international format started with + like +989123456789
    Change default SessionUserId name from session to a unique session file name for each user like session_9123456789
    Remove custom TcpClientConnectionHandler (with custom IP) and create TelegramClient with a null parameter as handler (actually just create TelegramClient with 4 parameters apiId, apiHash, store and sessionUserId without handler)
    Remove Telegram word from client title in https://my.telegram.org based on telegram terms

In my experience sequences are
*/
        private async void authorize()
        {
            try
            {
                updateStatus(null, Color.Black);

                string phone = Properties.Settings.Default.Phone;
                int api_id = Properties.Settings.Default.Api_Id;
                string api_hash = Properties.Settings.Default.Api_Hash;

                if (String.IsNullOrEmpty(phone) || String.IsNullOrEmpty(phone) || String.IsNullOrEmpty(phone))
                {
                    MessageBox.Show("Нужно ввести телефон, api_id, api_hash", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                _client = new TelegramClient(api_id, api_hash,
                    handler: new TLSharp.Core.Network.TcpClientConnectionHandler(TcpHandler));

                await _client.ConnectAsync();

                if (!await _client.IsPhoneRegisteredAsync(phone)) throw new Exception("Номер не зарегистрирован");

                if (!_client.IsUserAuthorized())
                {
                    string hash = await _client.SendCodeRequestAsync(phone);
                    string code = String.Empty;

                    DialogCode dialog = new DialogCode();
                    dialog.ShowDialog();

                    if (dialog.DialogResult == DialogResult.OK)
                    {
                        code = dialog.Code;
                        dialog.Dispose();
                    }
                    else
                        dialog.Dispose();

                    if (!String.IsNullOrEmpty(code))
                    {
                        await _client.MakeAuthAsync(phone, hash, code);

                        updateStatus("Авторизирован", Color.Green);

                        enableControls();
                    }
                }
            }
            catch (InvalidPhoneCodeException ex)
            {
                log.Error(ex);
                MessageBox.Show("CodeToAuthenticate is wrong, fill it with the code you just got now by SMS/Telegram", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                updateStatus(ex.Message, Color.Red);
            }
        }

        TcpClient TcpHandler(string address, int port)
        {
            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.ProxyHost)
                    || Properties.Settings.Default.ProxyPort < 1)
                return new TcpClient(address, port);
            //return new TcpClient("91.108.56.165", 443);

            ProxyClient proxyClient = null;
            string username = Properties.Settings.Default.ProxyUsername;
            string password = Properties.Settings.Default.ProxyPassword;

            switch (Properties.Settings.Default.ProxyType)
            {
                default:
                case (int)ProxyType.Http:
                    proxyClient = new HttpProxyClient(Properties.Settings.Default.ProxyHost, Properties.Settings.Default.ProxyPort, username, password);
                    break;

                case (int)ProxyType.Socks4:
                    proxyClient = new Socks4ProxyClient(Properties.Settings.Default.ProxyHost, Properties.Settings.Default.ProxyPort, username);
                    break;

                case (int)ProxyType.Socks4a:
                    proxyClient = new Socks4aProxyClient(Properties.Settings.Default.ProxyHost, Properties.Settings.Default.ProxyPort, username);
                    break;

                case (int)ProxyType.Socks5:
                    proxyClient = new Socks5ProxyClient(Properties.Settings.Default.ProxyHost, Properties.Settings.Default.ProxyPort, username, password);
                    break;

                case (int)ProxyType.Chain:
                    proxyClient = new ChainProxyClient();
                    break;
            }

            return proxyClient.CreateConnection(address, port);
        }

        private async Task checkQiwiInvoice(BotContext context, Order order)
        {
            try
            {
                if (qiwi == null)
                    return;

                History history = await qiwi.GetHistoryAsync();

                if (history != null && history.Payments != null)
                {
                    Payment payment = history.Payments.FirstOrDefault(x => x.Type == QiwiApi.Models.Enums.PaymentType.In &&
                                                                           x.Status == QiwiApi.Models.Enums.PaymentStatus.Success &&
                                                                           x.Comment == order.Comment);

                    if (payment != null)
                    {
                        order.Payed = payment.Total;

                        TimeSpan tsQiwi = DateTime.Now - payment.Date;
                        TimeSpan tsOrder = DateTime.Now - order.Din;

                        // просрочил время
                        if (tsQiwi.TotalHours > Properties.Settings.Default.TimeToWait ||
                            tsOrder.TotalHours > Properties.Settings.Default.TimeToWait)
                        {
                            cancelPackage(context, order);

                            order.Status = Order.Statuses.TimeOut;
                        }
                        else if (order.Payed < order.PackagePrice)
                        {
                            cancelPackage(context, order);

                            order.Status = Order.Statuses.PaidNotEnough;
                        }
                        else
                        {
                            order.Status = Order.Statuses.Payed;
                        }

                        await Task.Run(() => ckeckQiwiBalance());
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private async void updateOrdersTimeOut()
        {
            if (isCanceled)
                return;

            int timeForPay = Properties.Settings.Default.TimeToWait * 60;
            BotContext context = null;

            try
            {
                context = BotContext.Create();
                //List<Order> orders = context.Orders.Where(x => x.Status == Order.Statuses.WaitingForPay).ToList();
                foreach (User user in context.Users)
                {
                    foreach (Order order in user.Orders.Where(x => x.Status == Order.Statuses.WaitingForPay))
                    {
                        try
                        {
                            TimeSpan ts = DateTime.Now - order.Din;

                            if (ts.TotalHours > Properties.Settings.Default.TimeToWait)
                            {
                                cancelPackage(context, order);
                                order.Status = Order.Statuses.TimeOut;
                                order.IsDone = true;

                                context.SaveChanges();

                                //await SendMessageToTelegram(user, "Время оплаты вышло, заказ отменен.");
                                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                    "Время оплаты вышло, заказ отменен.");
                            }

                            if (!order.IsDone)
                            {
                                //TimeSpan? tsNotif = DateTime.Now - order.Date;

                                if ((order.Date == null && ts.Minutes >= 9)
                                     || (order.Date != null && (DateTime.Now - order.Date).Value.Minutes >= 9))
                                {
                                    order.Date = DateTime.Now;
                                    context.SaveChanges();

                                    int min = (order.Din.AddMinutes(timeForPay) - DateTime.Now).Minutes;
                                    int nn = ((int)Math.Round(min / 10.0)) * 10;

                                    if (nn > 9)
                                    {
                                        //await SendMessageToTelegram(user, "До конца резерва осталость " + nn.ToString() + " минут.");
                                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                                            "До конца резерва осталость " + nn.ToString() + " минут.");
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("updateOrdersTimeOut()", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("updateOrdersTimeOut()", ex);
            }
            finally
            {
                context?.Dispose();
            }
        }

        /*private async Task<bool> SendMessageToTelegram(User user, string text)
        {
            int i = 0;

            while(i <= 2)
            {
                try
                {
                    await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash },
                        text);
                    return true;
                }
                catch (Exception e)
                {     
                    if (i == 2)
                        log.Error(e);

                    Thread.Sleep(50);
                    i++;
                }  
            }

            return false;
        }*/

        private void cancelPackage(BotContext context, Order order)
        {
            order.Date = null;

            City city = context.Cities.FirstOrDefault(x => x.Name == order.City);

            if (city != null)
            {
                Domain.Region region = city.Regions.FirstOrDefault(x => x.Name == order.Region);

                if (region != null)
                {
                    Product product = region.Products.FirstOrDefault(x => x.Name == order.Product);
                    Package package = product.Packages.FirstOrDefault(x => x.Name == order.PackageName);

                    package.Addresses.Add(new Address(order.Address));
                }
            }
        }

        private async void ckeckQiwiBalance()
        {
            if (qiwi != null)
            {
                try
                {
                    if (CURRENCY_CURRENT == Currency.RUB)
                    {
                        Balances balances = await qiwi.GetBalancesAsync();
                        if (balances != null && balances.Rub != null)
                        {
                            labelQiwiBalance.Invoke((MethodInvoker)delegate
                            {
                                labelQiwiBalance.Text = balances.Rub.ToString() + " " + CURRENCY_NAME;
                            });

                            if (!String.IsNullOrEmpty(Properties.Settings.Default.CardTransfer) && Properties.Settings.Default.Limit > 0 && balances.Rub.Value >= Properties.Settings.Default.Limit)
                            {
                                decimal howMuch = balances.Rub.Value;

                                await qiwi.SendMoneyToWallet(Properties.Settings.Default.CardTransfer, howMuch);
                            }
                        }
                    }
                    else if (CURRENCY_CURRENT == Currency.KZT)
                    {
                        Balances balances = await qiwi.GetBalancesAsync();
                        if (balances != null && balances.Kzt != null)
                        {
                            labelQiwiBalance.Invoke((MethodInvoker)delegate
                            {
                                labelQiwiBalance.Text = balances.Kzt.ToString() + " " + CURRENCY_NAME;
                            });

                            if (!String.IsNullOrEmpty(Properties.Settings.Default.CardTransfer) && Properties.Settings.Default.Limit > 0 && balances.Rub.Value >= Properties.Settings.Default.Limit)
                            {
                                decimal howMuch = balances.Kzt.Value;

                                await qiwi.SendMoneyToWallet(Properties.Settings.Default.CardTransfer, howMuch);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
        }

        private void updateStatus(string status, Color color)
        {
            labelStatus.Invoke((MethodInvoker)delegate
            {
                labelStatus.Text = status;
                labelStatus.ForeColor = color;
            });
        }

        private void enableControls()
        {
            buttonStart.Invoke((MethodInvoker)delegate
            {
                buttonStart.Enabled = buttonAuthorize.Enabled = ToolStripMenuItemMenu.Enabled =
                ToolStripMenuItemSending.Enabled = ToolStripMenuItemSettings.Enabled = ToolStripMenuItemAdd.Enabled = true;

                buttonStop.Enabled = false;
            });
        }

        private void disableControls()
        {
            buttonStart.Enabled = buttonAuthorize.Enabled = ToolStripMenuItemMenu.Enabled =
                ToolStripMenuItemSending.Enabled = ToolStripMenuItemSettings.Enabled = ToolStripMenuItemAdd.Enabled = false;

            buttonStop.Enabled = true;
        }

        private string getMenu(BotContext context, User user)
        {
            string result = String.Empty;

            //иногда херзнакет кто тут что понаписывал или я тупой, получается CurOrder поставлен в оплачен, но в листе ордеров он не изменился.
            foreach (Order o in user.Orders)
            {
                if (o.Id == user.CurOrder.Id && o.Status == Order.Statuses.WaitingForPay)
                {
                    o.Address = user.CurOrder.Address;
                    o.City = user.CurOrder.City;
                    o.Comment = user.CurOrder.Comment;
                    o.Date = user.CurOrder.Date;
                    o.Din = user.CurOrder.Din;
                    o.IsDone = user.CurOrder.IsDone;
                    o.PackageName = user.CurOrder.PackageName;
                    o.PackagePrice = user.CurOrder.PackagePrice;
                    o.Payed = user.CurOrder.Payed;
                    o.Product = user.CurOrder.Product;
                    o.Region = user.CurOrder.Region;
                    o.Status = user.CurOrder.Status;
                    o.QiwiNumber = user.CurOrder.QiwiNumber;
                }
            }

            if (user.Orders != null && user.Orders.Any(x => x.Status == Order.Statuses.WaitingForPay))
            {
                /* if (user.payType == User.PayType.QIWI)
                 {
                     result += "\n" + "Qiwi оплата.";
                 }
                 if (user.payType == User.PayType.BlockChain)
                 {
                     result += "\n" + "Bitcoin оплата.";
                 } */
                result += "\n" + Emoji.GetNumber(1) + Emoji.GetNumber(1) + Emoji.GetNumber(1) + " - Проверить оплату.";
                result += "\n\n% - Отменить неоплаченный заказ и сделать новый.";
            }

            result += "\n# - В главное меню. \n";
            result += "+ - Прайс. \n";

            List<Domain.Menu> list = context.Menu.ToList();
            foreach (Domain.Menu menu in list)
            {
                result += menu.Sign + " - " + menu.Name + ".\n";
            }

            result += "\n";

            return result;
        }

        public async void SendMessage(string text)
        {
            if (_client != null && !_client.IsUserAuthorized())
            {
                MessageBox.Show("Сначала нужно авторизироваться", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                buttonStart.Invoke((MethodInvoker)delegate
                {
                    buttonStart.Enabled = false;
                });

                BotContext context = BotContext.Create();

                List<User> list = context.Users.ToList();

                foreach (User user in list)
                {
                    try
                    {
                        await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }
                    finally
                    {
                        Task.Delay(1500).Wait();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                buttonStart.Invoke((MethodInvoker)delegate
                {
                    buttonStart.Enabled = true;
                });
            }
        }

        public bool CheckKey()
        {
            bool result = false;

            if (!String.IsNullOrEmpty(Properties.Settings.Default.Key))
            {
                string key = Properties.Settings.Default.Key;
                string processor_id = CommonWrapper.KeyGen.GetProcessorID();
                string decrypt = CommonWrapper.KeyGen.Decrypt(key);

                result = !String.IsNullOrEmpty(key) && processor_id == decrypt;
            }

            return result;
        }

        private bool isSpam()
        {
            bool result = false;

            return result;
        }

        public string getComment(int count)
        {
            string result = String.Empty;

            Random rand = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < count; i++)
            {
                result += rand.Next(0, 9);
            }

            return result;
        }

        private void MarkMessageRead(TelegramClient client, TLAbsInputPeer peer, int id)
        {
            // An exception happens here but it's not fatal.
            try
            {
                var request = new TLRequestReadHistory();
                request.MaxId = id;
                request.Peer = peer;
                client.SendRequestAsync<bool>(request).Wait();
            }
            catch { }
        }

        private void UpdateReviewsDate()
        {
            while (!isCanceled)
            {
                if (DateTime.Now.Hour >= 1)
                {
                    Thread.Sleep(60000);
                    continue;
                }

                try
                {
                    using (BotContext context = BotContext.Create())
                    {
                        foreach (Review r in context.Reviews)
                        {
                            if (r.Din.Day < DateTime.Now.Day) r.Din = r.Din.AddDays((DateTime.Now - r.Din).TotalDays);
                        }

                        context.SaveChanges();
                    }

                    Thread.Sleep(60000);
                    //Task.Delay(60000);
                }
                catch (Exception e)
                {
                    log.Error("UpdateReviewsDate() = ", e);
                }
            }
        }

        #endregion

        #region ===messages===

        private User getUser(BotContext context, TLMessage message, TLUser _user)
        {
            //TLUser _user = diff.Users.ToList()
            //               .Where(x => x.GetType() == typeof(TLUser))
            //               .Cast<TLUser>()
            //               .FirstOrDefault(x => x.Id == message.FromId.Value);

            User user = context.Users.FirstOrDefault(x => x.ChatId == message.FromId.Value);

            if (user == null)
            {
                user = new User();
                user.ChatId = message.FromId.Value;
                user.Din = DateTime.Now;
                user.FirstName = _user.FirstName;
                user.LastName = _user.LastName;
                user.UserName = _user.Username;
                user.Action = User.Actions.New;

                context.Users.Add(user);
            }

            user.AccessHash = _user.AccessHash.Value;
            user.LastMessage = message.Date;

            user.CurOrder = user.Orders.FirstOrDefault(x => !x.IsDone);
            if (user.CurOrder == null)
            {
                user.CurOrder = new Order();
                user.Orders.Add(user.CurOrder);
            }

            initUsers(context.Users.ToList());

            return user;
        }

        private async Task sendPhoto(User user, string photoName, string greeting)
        {
            string pathPhoto = PATH + photoName;

            if (File.Exists(pathPhoto))
            {
                try
                {
                    var fileResult = (TLInputFile)
                    await _client.UploadFile(photoName, new StreamReader(pathPhoto));
                    await _client.SendUploadedPhoto(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, fileResult, greeting);
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
        }

        private async Task showMenu(BotContext context, User user)
        {
            List<City> cities = context.Cities.ToList();

            string text = String.Empty;

            if (!String.IsNullOrEmpty(Properties.Settings.Default.Greeting))
            {
                text += Properties.Settings.Default.Greeting + "\n";
                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + "\n";
            }

            text += Emoji.Raised_Hand + "Отзывы покупателей (отправьте " + Emoji.Point_Right + Emoji.GetNumber(9) + Emoji.GetNumber(9) + Emoji.GetNumber(9) + ") \n" +
                          Emoji.Thumbsup + "Оставить отзыв (отправьте " + Emoji.Point_Right + Emoji.GetNumber(7) + Emoji.GetNumber(7) + Emoji.GetNumber(7) + ") \n\n" +
                          "Для покупки отправьте цифру своего города из списка снизу: \n\n";

            int index = 1;
            foreach (City city in cities)
            {
                text += Emoji.GetNumber(index) + " " + city.Name + " \n";
                index++;
            }

            text += getMenu(context, user);

            user.Action = User.Actions.None;

            if (Properties.Settings.Default.IsGreeting)
                await sendPhoto(user, "greeting.jpeg", text);
            else
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        private async Task showMenuNew(BotContext context, Domain.User user, Domain.Menu menu)
        {
            string text = Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + "\n" +
                                                         menu.Page + "\n" +
                                                         Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + "\n";
            text += getMenu(context, user);

            user.Action = User.Actions.InMenu;

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        private async Task showBan(BotContext context, Domain.User user, Domain.Menu menu)
        {
            string text = "&nbsp;";
            //text += getMenu(context, user);

            user.Action = User.Actions.None;

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        private static Dictionary<string, int> amountRandom = new Dictionary<string, int>();

        private static DateTime timeOfLastAmountRandom = DateTime.MinValue;

        private async Task showPriceList(BotContext context, User user)
        {
            if ((DateTime.Now - timeOfLastAmountRandom).Hours > 1)
            {
                Dictionary<string, int> collection2 = new Dictionary<string, int>(amountRandom);

                foreach (string d in collection2.Keys)
                {
                    amountRandom[d] = rand.Next(5, 20);
                }

                timeOfLastAmountRandom = DateTime.Now;
            }


            List<City> cities = context.Cities.ToList();

            string text = "Сейчас в наличии: \n\n";

            foreach (City city in cities)
            {
                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        city.Name +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + "\n\n";

                Dictionary<string, HashSet<string>> dic = new Dictionary<string, HashSet<string>>();

                foreach (Domain.Region region in city.Regions)
                {
                    foreach (Product product in region.Products)
                    {
                        if (!dic.ContainsKey(product.Name))
                        {
                            dic.Add(product.Name, new HashSet<string>());
                        }

                        foreach (Package package in product.Packages)
                        {
                            string s = city.Name + product.Name + package.Name;

                            if (!amountRandom.ContainsKey(s))
                            {
                                amountRandom.Add(s, rand.Next(5, 20));
                            }

                            string str = package.Name + " за " + Math.Round(package.Price, 0)
                                + " " + CURRENCY_NAME + " - (" + amountRandom[s].ToString() + " шт.)";

                            dic[product.Name].Add(str);
                        }
                    }
                }

                foreach (KeyValuePair<string, HashSet<string>> d in dic)
                {
                    text += d.Key + "\n";
                    foreach (string s in d.Value)
                    {
                        text += s + "\n";
                    }
                    text += "\n";
                }
            }


            text += getMenu(context, user);

            user.Action = User.Actions.InPrice;
            if (text.Length > 4096)
            {
                foreach (string tt1 in text.SplitInParts(4096))
                {
                    await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, tt1);
                }
            }
            else
            {
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
        }

        private string _randomNumber = null;
        private Dictionary<string, int> _fileDefaultNumbers = new Dictionary<string, int>();
        private DateTime _dateOfGenerateRandomNumber = DateTime.MinValue;
        private DateTime _dateOfCheckChangeFile = DateTime.MinValue;

        private string GetQiwiNumber()
        {
            if ((DateTime.Now - _dateOfCheckChangeFile).Minutes >= 1)
            {
                var file = ReadFileList();

                if (file.Count != _fileDefaultNumbers?.Count
                    || file.All(s => _fileDefaultNumbers.ContainsKey(s.Key))) // is it right
                {
                    _dateOfGenerateRandomNumber = DateTime.MinValue;
                }

                _dateOfCheckChangeFile = DateTime.Now;
            }

            if (string.IsNullOrEmpty(_randomNumber) || (DateTime.Now - _dateOfGenerateRandomNumber).Hours >= 1)
            // in 00.48 - do not work
            {
                var file = ReadFileList();
                if (file.Count == 0)
                {
                    if (string.IsNullOrEmpty(_randomNumber))
                        return "";
                    else
                        return _randomNumber;
                }


                //добавляем неиспользованные
                var listForRand = new List<string>();
                foreach (KeyValuePair<string, int> ent in file)
                {
                    if (ent.Value == 0)
                        listForRand.Add(ent.Key);
                }

                //если нету свободных номер, добавляем уже занятые.
                if (listForRand.Count == 0)
                {
                    foreach (KeyValuePair<string, int> ent in file)
                        listForRand.Add(ent.Key);

                    if (listForRand.Count > 1 && listForRand.Contains(_randomNumber))
                    {
                        listForRand.Remove(_randomNumber);
                    }
                }

                //снять использование на текущий номер
                if (!string.IsNullOrEmpty(_randomNumber)
                     && file.ContainsKey(_randomNumber))
                {
                    file[_randomNumber] = 0;
                }

                //новое значение
                _randomNumber = listForRand.OrderBy(x => rand.Next(0, listForRand.Count - 1)).First();
                _dateOfGenerateRandomNumber = DateTime.Now;
                _dateOfCheckChangeFile = DateTime.Now;

                file[_randomNumber] = 1;
                _fileDefaultNumbers = file;
                WriteFileList(file);
            }

            return _randomNumber;
        }

        private void UnCheckQiwiCurrentNumber()
        {
            if (!string.IsNullOrEmpty(_randomNumber)
                && !string.IsNullOrEmpty(PATH_QIWI_FILE))
            {
                var file = ReadFileList();

                if (!string.IsNullOrEmpty(_randomNumber)
                        && file.ContainsKey(_randomNumber))
                {
                    file[_randomNumber] = 0;
                    WriteFileList(file);
                }
            }
            _randomNumber = null;
        }

        private string PATH_QIWI_FILE;

        private Dictionary<string, int> ReadFileList()
        {
            var numbers = new Dictionary<string, int>();

            for (int i = 0; i < 10; i++)
            {
                if (IsFileLocked(new FileInfo(PATH_QIWI_FILE)))
                {
                    Thread.Sleep(100 + rand.Next(1, 100));
                }

                FileStream stream = null;
                StreamReader file = null;

                try
                {
                    numbers = new Dictionary<string, int>();

                    stream = new FileStream(PATH_QIWI_FILE, FileMode.Open, FileAccess.ReadWrite);
                    file = new StreamReader(stream);

                    string line;
                    while (!string.IsNullOrEmpty(line = file.ReadLine()))
                    {
                        int delSymb = line.IndexOf('=');
                        if (delSymb != -1)
                        {
                            string numb = line.Substring(0, delSymb);
                            int.TryParse(line.Substring(delSymb + 1), out int usage);
                            if (usage == 1 || usage == 0)
                            {
                                if (!numbers.ContainsKey(numb))
                                    numbers.Add(numb, usage);
                                continue;
                            }
                        }

                        if (!numbers.ContainsKey(line))
                            numbers.Add(line, 0);
                    }

                    file?.Close();
                    stream?.Close();

                    break;
                }
                catch (Exception e)
                {
                    log.Error("ReadFileList()", e);

                }
                finally
                {
                    try
                    {
                        stream?.Close();
                        file?.Close();
                    }
                    catch { }
                }

                Thread.Sleep(100 + rand.Next(1, 100));
            }

            return numbers;
        }

        private void WriteFileList(Dictionary<string, int> dictionary)
        {
            var numbers = new Dictionary<string, int>();

            for (int i = 0; i < 20; i++)
            {
                if (IsFileLocked(new FileInfo(PATH_QIWI_FILE)))
                {
                    Thread.Sleep(50 + rand.Next(1, 100));
                }

                FileStream stream = null;
                StreamWriter writer = null;

                try
                {
                    stream = new FileStream(PATH_QIWI_FILE, FileMode.Create, FileAccess.ReadWrite);
                    writer = new StreamWriter(stream);

                    foreach (KeyValuePair<string, int> entry in dictionary)
                    {
                        /*if (entry.Value == 0)
                            writer.WriteLine(entry.Key);
                        else*/
                        writer.WriteLine(entry.Key + "=" + entry.Value);
                    }
                    stream.Flush();
                    writer.Flush();

                    stream?.Close();
                    //writer?.Close();
                    break;
                }
                catch (Exception e)
                {
                    log.Error("WriteFileList()", e);

                }
                finally
                {
                    try
                    {
                        stream?.Close();
                        writer?.Close();
                    }
                    catch { }
                }

                Thread.Sleep(100 + rand.Next(1, 100));
            }
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                log.Info("IsFileLocked() file locked.");
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        private async Task showOrders(BotContext context, User user, long id)
        {
            string text = "У вас нет заказов ожидающих оплату";

            if (user.CurOrder.Status == Order.Statuses.WaitingForPay)
            {
                await checkQiwiInvoice(context, user.CurOrder);

                // обработать заказ
                if (!user.CurOrder.IsDone)
                {
                    if (user.CurOrder.Status == Order.Statuses.PaidNotEnough)
                    {
                        user.CurOrder.IsDone = true;

                        text = Emoji.Tired_Face + " Вы перевели недостаточную сумму, либо Ваш платеж не найден.\n\n";
                    }
                    else if (user.CurOrder.Status == Order.Statuses.Payed)
                    {
                        user.CurOrder.IsDone = true;
#if !ALTERNATE
                        text = "Ваш платеж не найден, обратитесь к оператору";
#else
                        text = Emoji.Gift + " Вы оплатили заказ. \n\n" + user.CurOrder.Address + "\n";
#endif
                    }
                    else if (user.CurOrder.Status == Order.Statuses.TimeOut)
                    {
                        user.CurOrder.IsDone = true;

                        text = " Время на оплату заказа вышло. \n\n";
                    }
                    else if (user.CurOrder.Status == Order.Statuses.WaitingForPay && user.payType == User.PayType.QIWI)
                    {
                        //TODO мб вернуть
                        //text = "Список поступивших платежей обновляется раз в пять минут, пожалуйста, подождите...\n\n";

                        text = "Переведите на QIWI в течении " + Properties.Settings.Default.TimeToWait + " часов\n";

                        text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                                Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                                Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                                Emoji.Heavy_Minus_Sign + "\n" +
                                "Кошелек: +7" + user.CurOrder.QiwiNumber + "\n" +
                                "Сумма: " + Math.Round(user.CurOrder.PackagePrice) + " " + CURRENCY_NAME + " " + "\n" +
                                "Комментарий: " + user.CurOrder.Comment + "\n" +
                                Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                                Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                                Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                                Emoji.Heavy_Minus_Sign + "\n";

                        text += "БЕЗ КОММЕНТАРИЯ ДЕНЬГИ НЕ ЗАЧИСЛЯЮТСЯ\n";
                    }
                    else if (user.CurOrder.Status == Order.Statuses.WaitingForPay && user.payType == User.PayType.BlockChain)
                    {
                        text = "Переведите BTC в течении " + Properties.Settings.Default.TimeToWait + " часов\n";
                    }
                    else if (user.CurOrder.Status == Order.Statuses.WaitingForPay && user.payType == User.PayType.Card)
                    {
                        text = "Переведите на карту в течении " + Properties.Settings.Default.TimeToWait + " часов\n";
                    }
                }
            }

            text += getMenu(context, user);

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        private async Task showOrdersWEX(BotContext context, User user, string message)
        {
            string text = "Не верно указан wex-купон, формат WEX:код";
            var data = string.Empty;

            try
            {
                if (!message.Contains("WEX:")) throw new Exception();

                data = message.Split(':')[1];
            }
            catch
            {

            }

            if (!string.IsNullOrEmpty(data))
            {
                var wex = new WEX(data, user.CurOrder.PackagePrice);
                if (wex.Activate())
                {
#if !ALTERNATE
                    text = "Ваш платеж не найден, обратитесь к оператору";
#else
                    user.CurOrder.IsDone = true;
                    text = Emoji.Gift + " Вы оплатили заказ. \n\n" + user.CurOrder.Address + "\n";
                    user.payType = User.PayType.Other;
                    user.CurOrder.Status = Order.Statuses.Payed;
#endif
                }
                else
                {
                    text = $"Товар не оплачен, причины:\n- не верный wex купон\n- сумма купона меньше чем {user.CurOrder.Payed} " + CURRENCY_NAME;
                }
            }

            text += getMenu(context, user);

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        private async Task showReview(BotContext context, User user)
        {
            string text = "Напишите отзыв: ";

            if (!user.Orders.Any(x => x.Status == Order.Statuses.Payed))
                text = Emoji.No_Entry_Sign + " Отзывы можно оставлять только после покупок.";
            else
                user.Action = User.Actions.WaitingReview;

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        private async Task showReviewAll(BotContext context, User user)
        {
            //List<Review> reviews = context.Reviews.OrderByDescending(x => x.Din).ToList();
            string text = "";

            int con = context.Reviews.Count();
            List<Review> reviews = context.Reviews.OrderBy(x => rand.Next(con)).Take(5).ToList();

            if (!reviews.Any()) text = "Еще нету отзывов.";

            text += Emoji.Raised_Hand + "Показать еще (отправьте " + Emoji.Point_Right + Emoji.GetNumber(9) + Emoji.GetNumber(9) + Emoji.GetNumber(9) + ")\n\n";
            foreach (Review review in reviews)
            {
                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        "ОТ " + review.From + " " + review.Din.ToShortDateString() +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + "\n" +
                        review.Text + "\n\n";
            }

            List<Domain.Menu> list = context.Menu.ToList();
            foreach (Domain.Menu menu in list)
            {
                text += menu.Sign + " - " + menu.Name + ".\n";
            }

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        //private Dictionary<long, KeyValuePair<DateTime, List<Domain.Region>>> cityRegions = new Dictionary<long, KeyValuePair<DateTime, List<Domain.Region>>>();
        private Dictionary<string, KeyValuePair<DateTime, List<Domain.Region>>> packageCityRegions
            = new Dictionary<string, KeyValuePair<DateTime, List<Domain.Region>>>();

        private async Task showRegions(BotContext context, User user, TLMessage message, bool b_ray)
        {
            List<City> cities = context.Cities.ToList();
            City city = null;
            string text = UNKNOWN_CODE;

            int index = 0;
            if (int.TryParse(message.Message, out index))
            {
                int _index = 1;
                foreach (City _city in cities)
                {
                    if (_index == index)
                    {
                        city = _city;
                        break;
                    }
                    _index++;
                }
            }

            if (city != null)
            {
                if (user.Orders.Any(x => x.Status == Order.Statuses.WaitingForPay))
                    text = "У вас есть заказ, ожидающий оплату. Оплатите или отмените его.";
                else
                {
                    user.CurOrder.City = city.Name;
                    user.Action = User.Actions.WaitingRegion;

                    text = "Вы выбрали \"" + city.Name + "\"\n\n";
                    text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + "\n" +
                            "Город: " + city.Name + "\n" +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + "\n";

                    text += "\nТовар: \n\n";
                    if (b_ray)
                    {
                        await showRegionProductsCity(context, user, message);
                        return;
                    }

                    //думаю что сюда никогда не доходит
                    /*int regionIndex = 1;
                    
                    if (!cityRegions.ContainsKey(city.Id) || cityRegions[city.Id].Value == null || cityRegions[city.Id].Value.Count == 0 || (DateTime.Now - cityRegions[city.Id].Key).Hours >= 1)
                    {
#if ALTERNATE
                        int count = city.Regions.Count;
#else
                        int max = 5;
                        int count = rand.Next(city.Regions.Count < 2 ? city.Regions.Count : 2, (city.Regions.Count <= max ? city.Regions.Count : max) + 1);
#endif
                        cityRegions[city.Id] = new KeyValuePair<DateTime, List<Domain.Region>>(DateTime.Now, city.Regions.OrderBy(x => rand.Next()).Take(count).ToList());
                        cityRegions[city.Id] 
                            = new KeyValuePair<DateTime, List<Domain.Region>>
                            (DateTime.Now, city.Regions.OrderBy(x => rand.Next()).Take(count).ToList());         
                     }

                    foreach (Domain.Region region in cityRegions[city.Id].Value)
                    {
                        text += Emoji.GetNumber(regionIndex) + " " + region.Name + "\n";
                        regionIndex++;
                    }

                    text += getMenu(context, user);
*/
                }
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else
            {
                await showUnknown(context, user);
            }
        }

        private async Task showRegionsNEW(BotContext context, User user, TLMessage message)
        {
            City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);

            string text = UNKNOWN_CODE;
            string text1 = "";
            int packageIndex = 1;

            foreach (Domain.Region region in city.Regions)
            {
                foreach (Product product1 in region.Products)
                {
                    if (product1.Name == user.CurOrder.Product)
                    {
                        foreach (Package package in product1.Packages)
                        {
                            if (!text1.Contains(";" + package.Name + ";" + Math.Round(package.Price, 0) + ";"))
                                text1 += Emoji.GetNumber(packageIndex) + ";" + package.Name + ";" + Math.Round(package.Price, 0) + ";";
                            packageIndex++;
                        }
                    }
                }
            }
            string[] findprodukt = text1.Split(new char[] { ';' }, StringSplitOptions.None);
            int index = 0;

            if (int.TryParse(message.Message, out index))
            {
                user.CurOrder.PackageName = findprodukt[index * 3 - 2];
                user.CurOrder.PackagePrice = Convert.ToDecimal(findprodukt[index * 3 - 1]);
            }

            user.Action = User.Actions.WaitingPackageNew;

            text = "Вы выбрали \"" + user.CurOrder.PackageName + " за " + Math.Round(user.CurOrder.PackagePrice, 0) + " " + CURRENCY_NAME + "\"\n\n";

            text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                    Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                    Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                    Emoji.Heavy_Minus_Sign + "\n" +
                    "Город: " + user.CurOrder.City + "\n" +
                    "Товар: " + user.CurOrder.Product + "\n" +
                    "Фасовка: " + user.CurOrder.PackageName + " за " + Math.Round(user.CurOrder.PackagePrice, 0) + " " + CURRENCY_NAME + "\n" +
                    Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                    Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                    Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                    Emoji.Heavy_Minus_Sign + "\n";

            text += "\nВыберите Район: \n\n";

            int regionIndex = 1;

            /* if (!cityRegions.ContainsKey(city.Id) || cityRegions[city.Id].Value == null || cityRegions[city.Id].Value.Count == 0 || 
                 (DateTime.Now - cityRegions[city.Id].Key).Hours >= 1)
             {
                 //int count = city.Regions.Count;
                 int max = 5;
                 int count = rand.Next(city.Regions.Count < 2 ? city.Regions.Count : 2, (city.Regions.Count <= max ? city.Regions.Count : max) + 1);
                 cityRegions[city.Id] = new KeyValuePair<DateTime, List<Domain.Region>>(DateTime.Now, city.Regions.OrderBy(x => rand.Next()).Take(count).ToList());
             }*/

            string cityIdUserCurProd = city.Id + user.CurOrder.Product;

            if (!packageCityRegions.ContainsKey(cityIdUserCurProd)
                || packageCityRegions[cityIdUserCurProd].Value == null
                || (DateTime.Now - packageCityRegions[cityIdUserCurProd].Key).Hours >= 1)
            {
                int max = 5;
                int count = rand.Next(
                    city.Regions.Count < 2 ? city.Regions.Count : 2,
                    (city.Regions.Count <= max ? city.Regions.Count : max) + 1
                    );
                packageCityRegions[cityIdUserCurProd]
                    = new KeyValuePair<DateTime, List<Domain.Region>>
                    (DateTime.Now, city.Regions.OrderBy(x => rand.Next())
                    .Take(count).ToList());
            }

            bool logR = false;
            foreach (Domain.Region region in packageCityRegions[cityIdUserCurProd].Value)
            {
                foreach (Product product1 in region.Products)
                {
                    if (product1.Name == user.CurOrder.Product)
                    {
                        foreach (Package package in product1.Packages)
                        {
                            if (package.Name == user.CurOrder.PackageName && package.Price == user.CurOrder.PackagePrice)
                            {
                                logR = true;
                            }
                        }
                    }
                }
                if (logR)
                {
                    text += Emoji.GetNumber(regionIndex) + " " + region.Name + "\n";
                    logR = false;
                    regionIndex++;
                }
            }

            text += getMenu(context, user);

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        private async Task showRegionProductsCity(BotContext context, User user, TLMessage message)
        {
            City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);
            string text = UNKNOWN_CODE;
            text = "";
            text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n" +
                        "Город: " + city.Name + "\n" +
                        //      "Район: " + region.Name + "\n" +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

            text += "\nВыберите товар: \n\n";

            int productIndex = 1;
            string text1 = "";
            user.Action = User.Actions.WaitingProduct;

            foreach (Domain.Region region in city.Regions)
            {
                foreach (Product product in region.Products)
                {
                    if (!text1.Contains(" " + product.Name + "\n"))
                        text1 += Emoji.GetNumber(productIndex) + " " + product.Name + "\n";
                    productIndex++;
                }
            }
            text += text1;
            text += getMenu(context, user);

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }

        /*
        private async Task showRegionProducts(BotContext context, User user, TLMessage message)
        {
            City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);

            Domain.Region region = null;
            string text = UNKNOWN_CODE;

            int index = 0;
            if (int.TryParse(message.Message, out index))
            {
                int _index = 1;
                foreach (Domain.Region _region in cityRegions[city.Id].Value)
                {
                    if (_index == index)
                    {
                        region = _region;

                        break;
                    }

                    _index++;
                }
            }

            if (region != null)
            {
                user.Action = User.Actions.WaitingProduct;
                user.CurOrder.Region = region.Name;

                text = "Вы выбрали \"" + region.Name + "\"\n\n";

                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n" +
                        "Город: " + city.Name + "\n" +
                        "Район: " + region.Name + "\n" +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                text += "\nВыберите товар: \n\n";

                int productIndex = 1;
                foreach (Product product in region.Products)
                {
                    text += Emoji.GetNumber(productIndex) + " " + product.Name + "\n";
                    productIndex++;
                }
                text += getMenu(context, user);
            }

            await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
        }
        */

        private async Task showPackages(BotContext context, User user, TLMessage message, Domain.Menu menu)
        {
            City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);
            Domain.Region region = city.Regions.FirstOrDefault(x => x.Name == user.CurOrder.Region);
            Product product = null;
            string text = UNKNOWN_CODE;

            int index = 0;
            if (int.TryParse(message.Message, out index))
            {
                int _index = 1;
                foreach (Product _product in region.Products)
                {
                    if (_index == index)
                    {
                        product = _product;
                        break;
                    }
                    _index++;
                }
            }

            if (product != null)
            {
                user.CurOrder.Product = product.Name;

                user.Action = User.Actions.WaitingPackage;
                context.SaveChanges();

                text = "Вы выбрали \"" + product.Name + "\"\n\n";

                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n" +
                            "Город: " + city.Name + "\n" +
                            "Район: " + region.Name + "\n" +
                            "Товар: " + product.Name + "\n" +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                text += "\nВыберите фасовку: \n\n";

                int packageIndex = 1;
                foreach (Package package in product.Packages)
                {
                    text += Emoji.GetNumber(packageIndex) + " " + package.Name + " за " + Math.Round(package.Price, 0) + " " + CURRENCY_NAME + "\n";
                    packageIndex++;
                }

                text += getMenu(context, user);

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (menu != null)
            {
                await showMenuNew(context, user, menu);
            }
            else
            {
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, UNKNOWN_CODE);
            }
        }

        private async Task showPackagesNew(BotContext context, User user, TLMessage message, Domain.Menu menu)
        {
            City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);

            string text = UNKNOWN_CODE;
            string text1 = "";
            int productIndex = 1;
            foreach (Domain.Region region in city.Regions)
            {
                foreach (Product product1 in region.Products)
                {
                    if (!text1.Contains(";" + product1.Name + ";"))
                        text1 += Emoji.GetNumber(productIndex) + ";" + product1.Name + ";";
                    productIndex++;
                }
            }
            string[] findprodukt = text1.Split(new char[] { ';' }, StringSplitOptions.None);
            int index = 0;
            string product_name = "";
            if (int.TryParse(message.Message, out index))
            {
                //int _index = 1;
                //foreach (string _product in findprodukt)
                //{
                //    if (Emoji.GetNumber(_index) == _product)
                //    {
                //if (findprodukt.Length<= index * 2)
                product_name = findprodukt[index * 2 - 1];
                //        break;
                //    }
                //    _index++;
                //}
            }

            if (product_name != null)
            {
                //null d:\Projects\FreelanceHunt\bot_1_Универсальный\TelegramBotStore\Main.cs:строка 2264

                user.CurOrder.Product = product_name;
                user.Action = User.Actions.WaitingPackage;

                text = "Вы выбрали \"" + product_name + "\"\n\n";

                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n" +
                            "Город: " + city.Name + "\n" +
                            //"Район: " + region.Name + "\n" +
                            "Товар: " + product_name + "\n" +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                text += "\nВыберите фасовку: \n\n";

                text1 = "";
                int packageIndex = 1;
                foreach (Domain.Region region in city.Regions)
                {
                    foreach (Product product1 in region.Products)
                    {
                        if (product1.Name == user.CurOrder.Product)
                        {
                            foreach (Package package in product1.Packages)
                            {
                                if (!text1.Contains(" " + package.Name + " за " + Math.Round(package.Price, 0) + " " + CURRENCY_NAME))
                                    text1 += Emoji.GetNumber(packageIndex) + " " + package.Name + " за " + Math.Round(package.Price, 0) + " " + CURRENCY_NAME + "\n";
                                packageIndex++;
                            }
                        }
                    }
                }

                text += text1;
                text += getMenu(context, user);

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (menu != null)
            {
                await showMenuNew(context, user, menu);
            }
            else
            {
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, UNKNOWN_CODE);
            }
        }

        private async Task showPaymentNew(BotContext context, User user, TLMessage message, Domain.Menu menu)
        {
            //await showRegionsNEW(context, user, message);
            City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);
            Domain.Region region = null;


            //че за херь сдесь творится
            string text = UNKNOWN_CODE;
            string text1 = "";
            int index = 0;
            if (int.TryParse(message.Message, out index))
            {
                bool logR = false;
                foreach (Domain.Region region1 in packageCityRegions[city.Id + user.CurOrder.Product].Value)
                {
                    foreach (Product product1 in region1.Products)
                    {
                        if (product1.Name == user.CurOrder.Product)
                        {
                            foreach (Package package1 in product1.Packages)
                            {
                                if (package1.Name == user.CurOrder.PackageName && package1.Price == user.CurOrder.PackagePrice)
                                {
                                    logR = true;
                                }
                            }
                        }
                    }
                    if (logR)
                    {
                        text1 += region1.Name + ";";
                        logR = false;
                    }
                }
            }

            string[] regi = text1.Split(new char[] { ';' }, StringSplitOptions.None);
            if (index > 0)
                region = city.Regions.FirstOrDefault(x => x.Name == regi[index - 1]);

            Product product = null;
            try
            {
                product = region.Products.FirstOrDefault(x => x.Name == user.CurOrder.Product);
            }
            catch (NullReferenceException e)
            {
                log.Error(e);
                product = null;
            }

            Package package = product.Packages.FirstOrDefault(x => x.Name == user.CurOrder.PackageName);

            if (region != null)
            {
                if (package.Addresses.Any())
                {
                    user.CurOrder.Region = region.Name;
                    user.CurOrder.PackageName = package.Name;
                    user.CurOrder.PackagePrice = package.Price;

                    Domain.Address address = package.Addresses.First();
                    user.CurOrder.Address = address.Name;
                    package.Addresses.Remove(address);
                    if (package.Addresses.Count < 5)
                        package.Addresses.Add(new Domain.Address(getComment(6)));

                    user.Action = User.Actions.WaitingPaymentMethod;

                    text = "Вы выбрали \"" + region.Name + "\"\n\n";

                    text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + "\n" +
                               "Город: " + city.Name + "\n" +
                               "Район: " + region.Name + "\n" +
                               "Товар: " + user?.CurOrder?.Product /*product.Name*/ + "\n" +
                               "Фасовка: " + package.Name + " за " + Math.Round(package.Price, 0) + " " + CURRENCY_NAME + "\n" +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + "\n";

                    text += "\nВыберите способ оплаты: \n\n";

                    text += Emoji.GetNumber(1) + " QIWI\n";

                    if (IsWexAvailable)
                    {
                        text += Emoji.GetNumber(2) + " WEX-код\n";
                    }

                    if (IsBlockChainAvailable)
                    {
                        text += Emoji.GetNumber(3) + " Bitcoin";
                        if (_discountBTC > 0)
                        {
                            text += "(Скидка " + _discountBTC + "%)";
                        }
                        text += "\n";
                    }

                    if (IsCardAvailable)
                    {
                        text += Emoji.GetNumber(4) + " Перевести на карту.\n";
                    }

                    text += getMenu(context, user);
                }
                else
                {
                    text = "\nЗакончились товары, приходите чуть позже.";

                    user.Action = User.Actions.None;
                    user.CurOrder.City = String.Empty;
                    user.CurOrder.Region = String.Empty;
                    user.CurOrder.Product = String.Empty;
                    user.CurOrder.PackageName = String.Empty;
                    user.CurOrder.PackagePrice = 0;

                    text += getMenu(context, user);
                }

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (menu != null)
            {
                await showMenuNew(context, user, menu);
            }
            else
            {
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, UNKNOWN_CODE);
            }
        }

        private async Task showPayment(BotContext context, User user, TLMessage message, Domain.Menu menu)
        {

            await showRegionsNEW(context, user, message);
            City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);
            Domain.Region region = city.Regions.FirstOrDefault(x => x.Name == user.CurOrder.Region);
            Product product = region.Products.FirstOrDefault(x => x.Name == user.CurOrder.Product);
            Package package = null;
            string text = UNKNOWN_CODE;

            int index = 0;
            if (int.TryParse(message.Message, out index))
            {
                int _index = 1;
                foreach (Package _package in product.Packages)
                {
                    if (_index == index)
                    {
                        package = _package;
                        break;
                    }
                    _index++;
                }
            }

            if (package != null)
            {
                if (package.Addresses.Any())
                {
                    user.CurOrder.PackageName = package.Name;
                    user.CurOrder.PackagePrice = package.Price;

                    Domain.Address address = package.Addresses.First();
                    user.CurOrder.Address = address.Name;
                    package.Addresses.Remove(address);

                    user.Action = User.Actions.WaitingPaymentMethod;

                    text = "Вы выбрали \"" + package.Name + " за " + Math.Round(package.Price, 0) + " " + CURRENCY_NAME + "\"\n\n";

                    text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + "\n" +
                               "Город: " + city.Name + "\n" +
                               "Район: " + region.Name + "\n" +
                               "Товар: " + product.Name + "\n" +
                               "Фасовка: " + package.Name + " за " + Math.Round(package.Price, 0) + " " + CURRENCY_NAME + "\n" +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                            Emoji.Heavy_Minus_Sign + "\n";

                    text += "\nВыберите способ оплаты: \n\n";

                    text += Emoji.GetNumber(1) + " QIWI\n";

                    if (IsWexAvailable)
                    {
                        text += Emoji.GetNumber(2) + " WEX-код\n";
                    }
                    else if (IsBlockChainAvailable)
                    {
                        text += Emoji.GetNumber(3) + " Bitcoin";
                        if (_discountBTC > 0)
                        {
                            text += "(Скидка " + _discountBTC + "%)";
                        }
                    }

                    text += getMenu(context, user);
                }
                else
                {
                    text = "\nЗакончились товары, приходите чуть позже.";

                    user.Action = User.Actions.None;
                    user.CurOrder.City = String.Empty;
                    user.CurOrder.Region = String.Empty;
                    user.CurOrder.Product = String.Empty;
                    user.CurOrder.PackageName = String.Empty;
                    user.CurOrder.PackagePrice = 0;

                    text += getMenu(context, user);
                }

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (menu != null)
            {
                await showMenuNew(context, user, menu);
            }
            else
            {
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, UNKNOWN_CODE);
            }
        }

        private double courseDollar = 0;
        private DateTime courseDollarUpdateDate;

        private async Task showOrdered(BotContext context, User user, TLMessage message, Domain.Menu menu)
        {
            string text = UNKNOWN_CODE;

            if (message.Message == "1")
            {
                City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);
                Domain.Region region = city.Regions.FirstOrDefault(x => x.Name == user.CurOrder.Region);
                Product product = region.Products.FirstOrDefault(x => x.Name == user.CurOrder.Product);
                Package package = product.Packages.FirstOrDefault(x => x.Name == user.CurOrder.PackageName);

                user.CurOrder.Comment = getComment(6);

                text = "Переведите на QIWI в течении " + Properties.Settings.Default.TimeToWait + " часов\n";

                user.CurOrder.QiwiNumber = GetQiwiNumber();

                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n" +
                        "Кошелек: +7" + user.CurOrder.QiwiNumber + "\n" +
                        "Сумма: " + Math.Round(package.Price) + " " + CURRENCY_NAME + " " + "\n" +
                        "Комментарий: " + user.CurOrder.Comment + "\n" +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                text += "БЕЗ КОММЕНТАРИЯ ДЕНЬГИ НЕ ЗАЧИСЛЯЮТСЯ\n";

                user.Action = User.Actions.None;
                user.payType = User.PayType.QIWI;
                user.CurOrder.Status = Order.Statuses.WaitingForPay;
                user.CurOrder.Din = DateTime.Now;

                text += getMenu(context, user);

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (message.Message == "2" && IsWexAvailable)
            {
                City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);
                Domain.Region region = city.Regions.FirstOrDefault(x => x.Name == user.CurOrder.Region);
                Product product = region.Products.FirstOrDefault(x => x.Name == user.CurOrder.Product);
                Package package = product.Packages.FirstOrDefault(x => x.Name == user.CurOrder.PackageName);

                text = "Напишите WEX-код в течении " + Properties.Settings.Default.TimeToWait + " часов\n";

                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n" +
                        "Сумма: " + Math.Round(package.Price) + " " + CURRENCY_NAME + " " + "\n" +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                text += "Перед Wex-кодом введите 'WEX:'\n\nВведите WEX-код в таком виде - WEX:WEXRURXX01234567890AOIUAAABBBBI9JABOV6YNULCWKM!\n\n";
                text += "Мы принимаем только рублевые коды - WEXRUR\nКупить WEX-код можно купить на бирже https://wex.nz\n";
                text += "Получить WEX-код можно на большинстве on-line обменников.\n";
                text += "Загляни на https://www.bestchange.ru/qiwi-to-wex-rub.html чтобы найти подходящий по условиям.\n";
                text += "Внимание! Нужно прислать WEX-код на сумму, не менее, чем указанную при заказе! WEX-код обязательно должен быть в валюте RUR, как указано в примере выше!\n";
                user.Action = User.Actions.None;
                user.payType = User.PayType.WEX;
                user.CurOrder.Status = Order.Statuses.WaitingForPay;
                user.CurOrder.Din = DateTime.Now;

                text += getMenu(context, user);

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (message.Message == "3" && IsBlockChainAvailable)
            {
                initBlockChain();
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

                //???? if it not in the log file delete if
                if (user == null || user.CurOrder == null || user.CurOrder.PackagePrice == 0)
                {
                    City city = context.Cities.FirstOrDefault(x => x.Name == user.CurOrder.City);
                    Domain.Region region = city.Regions.FirstOrDefault(x => x.Name == user.CurOrder.Region);
                    Product product = region.Products.FirstOrDefault(x => x.Name == user.CurOrder.Product);
                    Package package = product.Packages.FirstOrDefault(x => x.Name == user.CurOrder.PackageName);
                    user.CurOrder.PackagePrice = package.Price;

                    log.Info("user == null || user.CurOrder == null || user.CurOrder.PackagePrice == 0 was runned.");
                }

                string btcAmountStr = "";

                if (CURRENCY_CURRENT == Currency.KZT)
                {
                    //convert from tenge to USD
                    //get usd course
                    if (courseDollar == 0 || (DateTime.Now - courseDollarUpdateDate).Hours >= 1)
                    {
                        string xmlStr = null;
                        using (var wc = new System.Net.WebClient())
                        { xmlStr = wc.DownloadString("http://www.nationalbank.kz/rss/rates_all.xml"); }

                        XDocument xdoc = XDocument.Parse(xmlStr);
                        var items = from xe in xdoc.Descendants("item")
                                    where xe.Element("title").Value == "USD"
                                    select xe.Element("description");

                        log.Info("Currency course =" + items.First().Value);
                        courseDollar = Double.Parse(items.First().Value.ToString(), CultureInfo.InvariantCulture.NumberFormat);
                        courseDollarUpdateDate = DateTime.Now;
                    }
                    double sum = (double)user.CurOrder.PackagePrice;
                    if (_discountBTC > 0)
                    {
                        sum = sum - (sum * (double)_discountBTC / 100);
                    }
                    //to usd
                    sum /= courseDollar;

                    //from RUB to bitcoin
                    ExchangeRateExplorer explorer = new ExchangeRateExplorer();
                    btcAmountStr = ((decimal)explorer.ToBtcAsync("USD", sum).Result).ToString();
                }
                else if (CURRENCY_CURRENT == Currency.RUB)
                {
                    double sum = (double)user.CurOrder.PackagePrice;
                    if (_discountBTC > 0)
                    {
                        sum = sum - (sum * (double)_discountBTC / 100);
                    }
                    //from RUB to bitcoin
                    ExchangeRateExplorer explorer = new ExchangeRateExplorer();
                    btcAmountStr = ((decimal)explorer.ToBtcAsync("RUB", sum).Result).ToString();
                }
                /* QueryString queryString = new QueryString();
                    queryString.Add("currency", "USD");
                    queryString.Add("value", sum.ToString(CultureInfo.InvariantCulture));
                    var httpClient = new BlockchainHttpClient();
                    var dd = await httpClient.GetAsync<double>("tobtc", queryString);
                */

                text = "Переведите BTC в течении " + Properties.Settings.Default.TimeToWait + " часов\n";
                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                //Receive receive = new Receive(new BlockchainHttpClient(null, "https://api.blockchain.info/v2/"));
                //ReceivePaymentResponse response =
                //    await receive.GenerateAddressAsync(_blockChainXPub, _blockChainCallback, _blockChainApiKey, _blockChainIntGapLimit);
                //text += "Переведите BTC на: \n" + response.Address.ToString() + "\n" +

                text += "Переведите BTC на: \n" + _blockChainAdderess + "\n" +
               "Сумма: " + btcAmountStr + "\n";

                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                //user.CurOrder.Comment = btcAmountStr + ", " + response.Address.ToString();
                user.CurOrder.Comment = btcAmountStr + ", " + _blockChainAdderess;

                user.Action = User.Actions.None;
                user.payType = User.PayType.BlockChain;
                user.CurOrder.Status = Order.Statuses.WaitingForPay;
                user.CurOrder.Din = DateTime.Now;

                text += getMenu(context, user);

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (message.Message == "4" && IsCardAvailable)
            {
                user.CurOrder.Comment = getComment(6);

                text = "Переведите на карту в течении " + Properties.Settings.Default.TimeToWait + " часов\n";

                text += Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n" +
                        "Карта: " + Properties.Settings.Default.CardNumber + "\n" +
                        "Сумма: " + Math.Round(user.CurOrder.PackagePrice) + " " + CURRENCY_NAME + " " + "\n" +
                        "Комментарий: " + user.CurOrder.Comment + "\n" +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign + Emoji.Heavy_Minus_Sign +
                        Emoji.Heavy_Minus_Sign + "\n";

                text += "БЕЗ КОММЕНТАРИЯ ДЕНЬГИ НЕ ЗАЧИСЛЯЮТСЯ\n";

                user.Action = User.Actions.None;
                user.payType = User.PayType.Card;
                user.CurOrder.Status = Order.Statuses.WaitingForPay;
                user.CurOrder.Din = DateTime.Now;

                text += getMenu(context, user);

                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
            else if (menu != null)
            {
                await showMenuNew(context, user, menu);
            }
            else
            {
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, UNKNOWN_CODE);
            }
        }

        private async Task showUnknown(BotContext context, User user)
        {
            string text = UNKNOWN_CODE;

            if (user.Action != User.Actions.InMenu && user.Action != User.Actions.InPrice)
            {
                await showMenu(context, user);
            }
            else
            {
                await _client.SendMessageAsync(new TLInputPeerUser() { UserId = user.ChatId, AccessHash = user.AccessHash }, text);
            }
        }

        #endregion

        #region ===init===

        private void initUsers(List<User> users = null)
        {
            dataGridViewUsers.Invoke((MethodInvoker)delegate
            {
                List<User> list = users != null ? users : BotContext.Create().Users.ToList();

                list = list.OrderByDescending(x => x.Din).ToList();

                dataGridViewUsers.DataSource = list;

                foreach (DataGridViewColumn column in dataGridViewUsers.Columns)
                {
                    switch (column.Name)
                    {
                        case "UserName":
                            column.HeaderText = "Никнейм";
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                        case "FirstName":
                            column.HeaderText = "Имя";
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                        case "IsBantext":
                            column.HeaderText = "Бан";
                            column.Width = 50;
                            break;
                        case "Din":
                            column.HeaderText = "Дата";
                            column.Width = 125;
                            column.DefaultCellStyle.Format = "dd.MM.yyyy HH:mm:ss";
                            break;
                        default:
                            column.Visible = false;
                            break;
                    }
                }

                dataGridViewUsers.ClearSelection();

                DataGridViewImageColumn colEdit = new DataGridViewImageColumn();
                colEdit.Name = "ban";
                colEdit.HeaderText = "";
                colEdit.Width = 20;
                colEdit.Image = Properties.Resources.block;
                dataGridViewUsers.Columns.Add(colEdit);
            });
        }

        private void initOrders()
        {
            User user = selectedUserId.HasValue ? BotContext.Create().Users.FirstOrDefault(x => x.Id == selectedUserId.Value) : new User();

            List<Order> list = selectedUserId.HasValue ? user.Orders : new List<Order>();

            list = list.Where(x => x.Status != Order.Statuses.InProcess).OrderByDescending(x => x.Din).ToList();

            dataGridViewOrders.DataSource = list;

            foreach (DataGridViewColumn column in dataGridViewOrders.Columns)
            {
                switch (column.Name)
                {
                    case "City":
                        column.HeaderText = "Город";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.DisplayIndex = 0;
                        break;
                    case "Region":
                        column.HeaderText = "Район";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.DisplayIndex = 2;
                        break;
                    case "Product":
                        column.HeaderText = "Товар";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.DisplayIndex = 1;
                        break;
                    case "PackageFull":
                        column.HeaderText = "Фасовка";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.DisplayIndex = 2;
                        break;
                    case "Address":
                        column.HeaderText = "Куплено";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.DisplayIndex = 4;
                        break;
                    case "Payed":
                        column.HeaderText = "Оплатили";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.DisplayIndex = 5;
                        break;
                    case "Din":
                        column.HeaderText = "Дата";
                        column.Width = 115;
                        column.DefaultCellStyle.Format = "dd.MM.yyyy HH:mm:ss";
                        column.DisplayIndex = 6;
                        break;
                    case "Status":
                        column.HeaderText = "Статус";
                        column.Width = 100;
                        column.DisplayIndex = 7;
                        break;
                    case "Comment":
                        column.HeaderText = "Коментарий";
                        column.Width = 80;
                        column.DisplayIndex = 9;
                        break;
                    default:
                        column.Visible = false;
                        break;
                }
            }

            if (list.Any(x => x.Status == Order.Statuses.WaitingForPay))
            {
                foreach (DataGridViewColumn column in dataGridViewOrders.Columns)
                {
                    if (column.Name == "Status")
                    {
                        column.HeaderText = "Статус-" + user.payType.ToString();
                    }
                }
            }

            dataGridViewOrders.ClearSelection();
        }

        private void initRevies()
        {
            List<Review> list = BotContext.Create().Reviews.OrderByDescending(x => x.Din).ToList();

            dataGridViewReview.DataSource = list;

            foreach (DataGridViewColumn column in dataGridViewReview.Columns)
            {
                switch (column.Name)
                {
                    case "From":
                        column.HeaderText = "От кого";
                        column.Width = 100;
                        column.DisplayIndex = 0;
                        break;
                    case "Text":
                        column.HeaderText = "Отзыв";
                        column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        column.DisplayIndex = 1;
                        break;
                    case "Din":
                        column.HeaderText = "Дата";
                        column.Width = 125;
                        column.DefaultCellStyle.Format = "dd.MM.yyyy HH:mm:ss";
                        column.DisplayIndex = 2;
                        break;
                    default:
                        column.Visible = false;
                        break;
                }
            }

            dataGridViewReview.ClearSelection();

            DataGridViewImageColumn col = new DataGridViewImageColumn();
            col.Name = "remove";
            col.Width = 20;
            col.HeaderText = "";
            col.Image = Properties.Resources.cancel;
            dataGridViewReview.Columns.Add(col);
        }

        public void InitCities()
        {
            dataGridViewCities.Invoke((MethodInvoker)delegate
            {
                List<City> list = BotContext.Create().Cities.ToList();

                dataGridViewCities.DataSource = list;

                foreach (DataGridViewColumn column in dataGridViewCities.Columns)
                {
                    switch (column.Name)
                    {
                        case "Name":
                            column.HeaderText = "Название";
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                        default:
                            column.Visible = false;
                            break;
                    }
                }

                dataGridViewCities.ClearSelection();

                DataGridViewImageColumn col = new DataGridViewImageColumn();
                col.Name = "edit";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.edit;
                dataGridViewCities.Columns.Add(col);

                col = new DataGridViewImageColumn();
                col.Name = "remove";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.cancel;
                dataGridViewCities.Columns.Add(col);
            });
        }

        public void InitRegions()
        {
            BotContext context = BotContext.Create();

            List<Domain.Region> list = new List<Domain.Region>();

            if (selectedCityId.HasValue)
            {
                City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                list = city.Regions.ToList();
            }

            dataGridViewRegions.Invoke((MethodInvoker)delegate
            {
                dataGridViewRegions.DataSource = list;

                foreach (DataGridViewColumn column in dataGridViewRegions.Columns)
                {
                    switch (column.Name)
                    {
                        case "Name":
                            column.HeaderText = "Название";
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                        default:
                            column.Visible = false;
                            break;
                    }
                }

                dataGridViewRegions.ClearSelection();

                DataGridViewImageColumn col = new DataGridViewImageColumn();
                col.Name = "edit";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.edit;
                dataGridViewRegions.Columns.Add(col);

                col = new DataGridViewImageColumn();
                col.Name = "remove";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.cancel;
                dataGridViewRegions.Columns.Add(col);
            });
        }

        public void InitProducts()
        {
            BotContext context = BotContext.Create();

            List<Product> list = new List<Product>();

            if (selectedCityId.HasValue && selectedRegionId.HasValue)
            {
                City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == selectedRegionId.Value);

                list = region.Products.ToList();
            }

            dataGridViewProduct.Invoke((MethodInvoker)delegate
            {
                dataGridViewProduct.DataSource = list;

                foreach (DataGridViewColumn column in dataGridViewProduct.Columns)
                {
                    switch (column.Name)
                    {
                        case "Name":
                            column.HeaderText = "Название";
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                        default:
                            column.Visible = false;
                            break;
                    }
                }

                dataGridViewProduct.ClearSelection();

                DataGridViewImageColumn col = new DataGridViewImageColumn();
                col.Name = "edit";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.edit;
                dataGridViewProduct.Columns.Add(col);

                col = new DataGridViewImageColumn();
                col.Name = "remove";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.cancel;
                dataGridViewProduct.Columns.Add(col);
            });
        }

        public void InitPackages()
        {
            BotContext context = BotContext.Create();

            List<Package> list = new List<Package>();

            if (selectedProductId.HasValue)
            {
                City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == selectedRegionId.Value);
                Product product = region.Products.FirstOrDefault(x => x.Id == selectedProductId.Value);

                list = product.Packages.ToList();
            }

            dataGridViewPackaging.Invoke((MethodInvoker)delegate
            {
                dataGridViewPackaging.DataSource = list;

                foreach (DataGridViewColumn column in dataGridViewPackaging.Columns)
                {
                    switch (column.Name)
                    {
                        case "Name":
                            column.HeaderText = "Название";
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                        case "Price":
                            column.HeaderText = "Цена";
                            column.Width = 75;
                            break;
                        default:
                            column.Visible = false;
                            break;
                    }
                }

                dataGridViewPackaging.ClearSelection();

                DataGridViewImageColumn col = new DataGridViewImageColumn();
                col.Name = "edit";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.edit;
                dataGridViewPackaging.Columns.Add(col);

                col = new DataGridViewImageColumn();
                col.Name = "remove";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.cancel;
                dataGridViewPackaging.Columns.Add(col);
            });
        }

        public void InitAddresses()
        {
            BotContext context = BotContext.Create();

            List<Domain.Address> list = new List<Domain.Address>();

            if (selectedPackageId.HasValue)
            {
                City city = context.Cities.FirstOrDefault(x => x.Id == selectedCityId.Value);
                Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == selectedRegionId.Value);
                Product product = region.Products.FirstOrDefault(x => x.Id == selectedProductId.Value);
                Package package = product.Packages.FirstOrDefault(x => x.Id == selectedPackageId);

                list = package.Addresses.ToList();
            }

            dataGridViewAddress.Invoke((MethodInvoker)delegate
            {
                dataGridViewAddress.DataSource = list;

                foreach (DataGridViewColumn column in dataGridViewAddress.Columns)
                {
                    switch (column.Name)
                    {
                        case "Name":
                            column.HeaderText = "Название";
                            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            break;
                        default:
                            column.Visible = false;
                            break;
                    }
                }

                dataGridViewAddress.ClearSelection();

                DataGridViewImageColumn col = new DataGridViewImageColumn();
                col.Name = "edit";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.edit;
                dataGridViewAddress.Columns.Add(col);

                col = new DataGridViewImageColumn();
                col.Name = "remove";
                col.Width = 20;
                col.HeaderText = "";
                col.Image = Properties.Resources.cancel;
                dataGridViewAddress.Columns.Add(col);
            });
        }

        private void initQiwi()
        {
            try
            {
                string phone = Properties.Settings.Default.QiwiWallet;
                string api = Properties.Settings.Default.QiwiApi;

                if (qiwi == null && !String.IsNullOrEmpty(phone) && !String.IsNullOrEmpty(api))
                    qiwi = new Qiwi(phone, api);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private void initBlockChain()
        {
            try
            {
                _blockChainApiKey = Properties.Settings.Default.BlockChainApiKey;
                _blockChainXPub = Properties.Settings.Default.BlockChainXPub;
                _discountBTC = Properties.Settings.Default.DiscountBTC;
                _blockChainAdderess = Properties.Settings.Default.BblockChainAdderess;
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        #endregion
    }
}
