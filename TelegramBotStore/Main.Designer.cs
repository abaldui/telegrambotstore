﻿namespace TelegramBotStore
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItemAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAddCity = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAddRegiob = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAddProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAddPackaging = new System.Windows.Forms.ToolStripMenuItem();
            this.адресToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ALLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьКоментарийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemSending = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxControl = new System.Windows.Forms.GroupBox();
            this.labelQiwiBalance = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelControl = new System.Windows.Forms.Panel();
            this.buttonAuthorize = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewOrders = new System.Windows.Forms.DataGridView();
            this.groupBoxUsers = new System.Windows.Forms.GroupBox();
            this.dataGridViewUsers = new System.Windows.Forms.DataGridView();
            this.groupBoxOrders = new System.Windows.Forms.GroupBox();
            this.buttonRefreshComment = new System.Windows.Forms.Button();
            this.dataGridViewReview = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxCities = new System.Windows.Forms.GroupBox();
            this.dataGridViewCities = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewRegions = new System.Windows.Forms.DataGridView();
            this.groupBoxRegions = new System.Windows.Forms.GroupBox();
            this.dataGridViewPackaging = new System.Windows.Forms.DataGridView();
            this.groupBoxProduct = new System.Windows.Forms.GroupBox();
            this.dataGridViewProduct = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewAddress = new System.Windows.Forms.DataGridView();
            this.timerTimeToWait = new System.Windows.Forms.Timer(this.components);
            this.contextMenuOrder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemSetStatusOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.groupBoxControl.SuspendLayout();
            this.panelControl.SuspendLayout();
            this.tableLayoutPanelTop.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrders)).BeginInit();
            this.groupBoxUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).BeginInit();
            this.groupBoxOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReview)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxCities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCities)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRegions)).BeginInit();
            this.groupBoxRegions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPackaging)).BeginInit();
            this.groupBoxProduct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProduct)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAddress)).BeginInit();
            this.contextMenuOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemAdd,
            this.ToolStripMenuItemSettings,
            this.ToolStripMenuItemSending,
            this.ToolStripMenuItemMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ToolStripMenuItemAdd
            // 
            this.ToolStripMenuItemAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemAddCity,
            this.ToolStripMenuItemAddRegiob,
            this.ToolStripMenuItemAddProduct,
            this.ToolStripMenuItemAddPackaging,
            this.адресToolStripMenuItem,
            this.ALLToolStripMenuItem,
            this.добавитьКоментарийToolStripMenuItem});
            this.ToolStripMenuItemAdd.Image = global::TelegramBotStore.Properties.Resources.plus;
            this.ToolStripMenuItemAdd.Name = "ToolStripMenuItemAdd";
            this.ToolStripMenuItemAdd.Size = new System.Drawing.Size(91, 24);
            this.ToolStripMenuItemAdd.Text = "Добавить";
            // 
            // ToolStripMenuItemAddCity
            // 
            this.ToolStripMenuItemAddCity.Name = "ToolStripMenuItemAddCity";
            this.ToolStripMenuItemAddCity.Size = new System.Drawing.Size(189, 22);
            this.ToolStripMenuItemAddCity.Text = "Город";
            this.ToolStripMenuItemAddCity.Click += new System.EventHandler(this.ToolStripMenuItemAddCity_Click);
            // 
            // ToolStripMenuItemAddRegiob
            // 
            this.ToolStripMenuItemAddRegiob.Name = "ToolStripMenuItemAddRegiob";
            this.ToolStripMenuItemAddRegiob.Size = new System.Drawing.Size(189, 22);
            this.ToolStripMenuItemAddRegiob.Text = "Район";
            this.ToolStripMenuItemAddRegiob.Click += new System.EventHandler(this.ToolStripMenuItemAddRegiob_Click);
            // 
            // ToolStripMenuItemAddProduct
            // 
            this.ToolStripMenuItemAddProduct.Name = "ToolStripMenuItemAddProduct";
            this.ToolStripMenuItemAddProduct.Size = new System.Drawing.Size(189, 22);
            this.ToolStripMenuItemAddProduct.Text = "Товар";
            this.ToolStripMenuItemAddProduct.Click += new System.EventHandler(this.ToolStripMenuItemAddProduct_Click);
            // 
            // ToolStripMenuItemAddPackaging
            // 
            this.ToolStripMenuItemAddPackaging.Name = "ToolStripMenuItemAddPackaging";
            this.ToolStripMenuItemAddPackaging.Size = new System.Drawing.Size(189, 22);
            this.ToolStripMenuItemAddPackaging.Text = "Фасовка";
            this.ToolStripMenuItemAddPackaging.Click += new System.EventHandler(this.ToolStripMenuItemAddPackaging_Click);
            // 
            // адресToolStripMenuItem
            // 
            this.адресToolStripMenuItem.Name = "адресToolStripMenuItem";
            this.адресToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.адресToolStripMenuItem.Text = "Куплено";
            this.адресToolStripMenuItem.Click += new System.EventHandler(this.адресToolStripMenuItem_Click);
            // 
            // ALLToolStripMenuItem
            // 
            this.ALLToolStripMenuItem.Name = "ALLToolStripMenuItem";
            this.ALLToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.ALLToolStripMenuItem.Text = "Товар/Фасовка/цена";
            this.ALLToolStripMenuItem.Click += new System.EventHandler(this.товарУпаковкаценаToolStripMenuItem_Click);
            // 
            // добавитьКоментарийToolStripMenuItem
            // 
            this.добавитьКоментарийToolStripMenuItem.Name = "добавитьКоментарийToolStripMenuItem";
            this.добавитьКоментарийToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.добавитьКоментарийToolStripMenuItem.Text = "Добавить отзыв";
            this.добавитьКоментарийToolStripMenuItem.Click += new System.EventHandler(this.добавитьКоментарийToolStripMenuItem_Click);
            // 
            // ToolStripMenuItemSettings
            // 
            this.ToolStripMenuItemSettings.Image = global::TelegramBotStore.Properties.Resources.settings;
            this.ToolStripMenuItemSettings.Name = "ToolStripMenuItemSettings";
            this.ToolStripMenuItemSettings.Size = new System.Drawing.Size(99, 24);
            this.ToolStripMenuItemSettings.Text = "Настройки";
            this.ToolStripMenuItemSettings.Click += new System.EventHandler(this.ToolStripMenuItemSettings_Click);
            // 
            // ToolStripMenuItemSending
            // 
            this.ToolStripMenuItemSending.Image = global::TelegramBotStore.Properties.Resources.contact;
            this.ToolStripMenuItemSending.Name = "ToolStripMenuItemSending";
            this.ToolStripMenuItemSending.Size = new System.Drawing.Size(111, 24);
            this.ToolStripMenuItemSending.Text = "Оповещение";
            this.ToolStripMenuItemSending.Click += new System.EventHandler(this.ToolStripMenuItemSending_Click);
            // 
            // ToolStripMenuItemMenu
            // 
            this.ToolStripMenuItemMenu.Image = global::TelegramBotStore.Properties.Resources.list;
            this.ToolStripMenuItemMenu.Name = "ToolStripMenuItemMenu";
            this.ToolStripMenuItemMenu.Size = new System.Drawing.Size(73, 24);
            this.ToolStripMenuItemMenu.Text = "Меню";
            this.ToolStripMenuItemMenu.Click += new System.EventHandler(this.ToolStripMenuItemMenu_Click);
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 1;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.groupBoxControl, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelTop, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.groupBoxOrders, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanel1, 0, 2);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 28);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 4;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1008, 701);
            this.tableLayoutPanelMain.TabIndex = 1;
            // 
            // groupBoxControl
            // 
            this.groupBoxControl.Controls.Add(this.labelQiwiBalance);
            this.groupBoxControl.Controls.Add(this.label3);
            this.groupBoxControl.Controls.Add(this.panelControl);
            this.groupBoxControl.Controls.Add(this.labelStatus);
            this.groupBoxControl.Controls.Add(this.label1);
            this.groupBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxControl.Location = new System.Drawing.Point(3, 649);
            this.groupBoxControl.Name = "groupBoxControl";
            this.groupBoxControl.Size = new System.Drawing.Size(1002, 49);
            this.groupBoxControl.TabIndex = 0;
            this.groupBoxControl.TabStop = false;
            this.groupBoxControl.Text = "Управление";
            // 
            // labelQiwiBalance
            // 
            this.labelQiwiBalance.AutoSize = true;
            this.labelQiwiBalance.Location = new System.Drawing.Point(255, 15);
            this.labelQiwiBalance.Name = "labelQiwiBalance";
            this.labelQiwiBalance.Size = new System.Drawing.Size(10, 13);
            this.labelQiwiBalance.TabIndex = 4;
            this.labelQiwiBalance.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Баланс:";
            // 
            // panelControl
            // 
            this.panelControl.Controls.Add(this.buttonAuthorize);
            this.panelControl.Controls.Add(this.buttonStop);
            this.panelControl.Controls.Add(this.buttonStart);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl.Location = new System.Drawing.Point(690, 16);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(309, 30);
            this.panelControl.TabIndex = 2;
            // 
            // buttonAuthorize
            // 
            this.buttonAuthorize.Image = global::TelegramBotStore.Properties.Resources.account;
            this.buttonAuthorize.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAuthorize.Location = new System.Drawing.Point(112, 3);
            this.buttonAuthorize.Name = "buttonAuthorize";
            this.buttonAuthorize.Size = new System.Drawing.Size(63, 24);
            this.buttonAuthorize.TabIndex = 2;
            this.buttonAuthorize.Text = "Войти";
            this.buttonAuthorize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAuthorize.UseVisualStyleBackColor = true;
            this.buttonAuthorize.Click += new System.EventHandler(this.buttonAuthorize_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Image = global::TelegramBotStore.Properties.Resources.stop;
            this.buttonStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStop.Location = new System.Drawing.Point(181, 3);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(60, 24);
            this.buttonStop.TabIndex = 1;
            this.buttonStop.Text = "Стоп";
            this.buttonStop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Image = global::TelegramBotStore.Properties.Resources.start;
            this.buttonStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStart.Location = new System.Drawing.Point(247, 3);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(60, 24);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Старт";
            this.buttonStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(56, 28);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(73, 13);
            this.labelStatus.TabIndex = 1;
            this.labelStatus.Text = "Авторизация";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Статус:";
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 375F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTop.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.groupBoxUsers, 0, 0);
            this.tableLayoutPanelTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(1002, 217);
            this.tableLayoutPanelTop.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewOrders);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(378, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(621, 211);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Заказы";
            // 
            // dataGridViewOrders
            // 
            this.dataGridViewOrders.AllowUserToAddRows = false;
            this.dataGridViewOrders.AllowUserToDeleteRows = false;
            this.dataGridViewOrders.AllowUserToResizeColumns = false;
            this.dataGridViewOrders.AllowUserToResizeRows = false;
            this.dataGridViewOrders.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewOrders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewOrders.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewOrders.Name = "dataGridViewOrders";
            this.dataGridViewOrders.ReadOnly = true;
            this.dataGridViewOrders.RowHeadersVisible = false;
            this.dataGridViewOrders.Size = new System.Drawing.Size(615, 192);
            this.dataGridViewOrders.TabIndex = 2;
            this.dataGridViewOrders.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewOrders_CellMouseClick);
            // 
            // groupBoxUsers
            // 
            this.groupBoxUsers.Controls.Add(this.dataGridViewUsers);
            this.groupBoxUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxUsers.Location = new System.Drawing.Point(3, 3);
            this.groupBoxUsers.Name = "groupBoxUsers";
            this.groupBoxUsers.Size = new System.Drawing.Size(369, 211);
            this.groupBoxUsers.TabIndex = 0;
            this.groupBoxUsers.TabStop = false;
            this.groupBoxUsers.Text = "Пользователи";
            // 
            // dataGridViewUsers
            // 
            this.dataGridViewUsers.AllowUserToAddRows = false;
            this.dataGridViewUsers.AllowUserToDeleteRows = false;
            this.dataGridViewUsers.AllowUserToResizeColumns = false;
            this.dataGridViewUsers.AllowUserToResizeRows = false;
            this.dataGridViewUsers.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewUsers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUsers.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewUsers.Name = "dataGridViewUsers";
            this.dataGridViewUsers.ReadOnly = true;
            this.dataGridViewUsers.RowHeadersVisible = false;
            this.dataGridViewUsers.Size = new System.Drawing.Size(363, 192);
            this.dataGridViewUsers.TabIndex = 2;
            this.dataGridViewUsers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUsers_CellClick);
            // 
            // groupBoxOrders
            // 
            this.groupBoxOrders.Controls.Add(this.buttonRefreshComment);
            this.groupBoxOrders.Controls.Add(this.dataGridViewReview);
            this.groupBoxOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxOrders.Location = new System.Drawing.Point(3, 226);
            this.groupBoxOrders.Name = "groupBoxOrders";
            this.groupBoxOrders.Size = new System.Drawing.Size(1002, 194);
            this.groupBoxOrders.TabIndex = 1;
            this.groupBoxOrders.TabStop = false;
            this.groupBoxOrders.Text = "Отзывы";
            // 
            // buttonRefreshComment
            // 
            this.buttonRefreshComment.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.buttonRefreshComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRefreshComment.Location = new System.Drawing.Point(920, 15);
            this.buttonRefreshComment.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRefreshComment.Name = "buttonRefreshComment";
            this.buttonRefreshComment.Size = new System.Drawing.Size(79, 24);
            this.buttonRefreshComment.TabIndex = 3;
            this.buttonRefreshComment.Text = "Обновить";
            this.buttonRefreshComment.UseVisualStyleBackColor = true;
            this.buttonRefreshComment.Click += new System.EventHandler(this.buttonRefreshComment_Click);
            // 
            // dataGridViewReview
            // 
            this.dataGridViewReview.AllowUserToAddRows = false;
            this.dataGridViewReview.AllowUserToDeleteRows = false;
            this.dataGridViewReview.AllowUserToResizeColumns = false;
            this.dataGridViewReview.AllowUserToResizeRows = false;
            this.dataGridViewReview.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReview.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewReview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReview.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewReview.Name = "dataGridViewReview";
            this.dataGridViewReview.ReadOnly = true;
            this.dataGridViewReview.RowHeadersVisible = false;
            this.dataGridViewReview.Size = new System.Drawing.Size(996, 175);
            this.dataGridViewReview.TabIndex = 2;
            this.dataGridViewReview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReview_CellClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.groupBoxCities, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxRegions, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxProduct, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 4, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 426);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1002, 217);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // groupBoxCities
            // 
            this.groupBoxCities.Controls.Add(this.dataGridViewCities);
            this.groupBoxCities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxCities.Location = new System.Drawing.Point(3, 3);
            this.groupBoxCities.Name = "groupBoxCities";
            this.groupBoxCities.Size = new System.Drawing.Size(194, 211);
            this.groupBoxCities.TabIndex = 0;
            this.groupBoxCities.TabStop = false;
            this.groupBoxCities.Text = "Город";
            // 
            // dataGridViewCities
            // 
            this.dataGridViewCities.AllowUserToAddRows = false;
            this.dataGridViewCities.AllowUserToDeleteRows = false;
            this.dataGridViewCities.AllowUserToResizeColumns = false;
            this.dataGridViewCities.AllowUserToResizeRows = false;
            this.dataGridViewCities.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewCities.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewCities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCities.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewCities.Name = "dataGridViewCities";
            this.dataGridViewCities.ReadOnly = true;
            this.dataGridViewCities.RowHeadersVisible = false;
            this.dataGridViewCities.Size = new System.Drawing.Size(188, 192);
            this.dataGridViewCities.TabIndex = 3;
            this.dataGridViewCities.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCities_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewRegions);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(203, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(194, 211);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Район";
            // 
            // dataGridViewRegions
            // 
            this.dataGridViewRegions.AllowUserToAddRows = false;
            this.dataGridViewRegions.AllowUserToDeleteRows = false;
            this.dataGridViewRegions.AllowUserToResizeColumns = false;
            this.dataGridViewRegions.AllowUserToResizeRows = false;
            this.dataGridViewRegions.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewRegions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewRegions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewRegions.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewRegions.Name = "dataGridViewRegions";
            this.dataGridViewRegions.ReadOnly = true;
            this.dataGridViewRegions.RowHeadersVisible = false;
            this.dataGridViewRegions.Size = new System.Drawing.Size(188, 192);
            this.dataGridViewRegions.TabIndex = 3;
            this.dataGridViewRegions.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRegions_CellClick);
            // 
            // groupBoxRegions
            // 
            this.groupBoxRegions.Controls.Add(this.dataGridViewPackaging);
            this.groupBoxRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxRegions.Location = new System.Drawing.Point(603, 3);
            this.groupBoxRegions.Name = "groupBoxRegions";
            this.groupBoxRegions.Size = new System.Drawing.Size(194, 211);
            this.groupBoxRegions.TabIndex = 1;
            this.groupBoxRegions.TabStop = false;
            this.groupBoxRegions.Text = "Фасовка";
            // 
            // dataGridViewPackaging
            // 
            this.dataGridViewPackaging.AllowUserToAddRows = false;
            this.dataGridViewPackaging.AllowUserToDeleteRows = false;
            this.dataGridViewPackaging.AllowUserToResizeColumns = false;
            this.dataGridViewPackaging.AllowUserToResizeRows = false;
            this.dataGridViewPackaging.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewPackaging.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewPackaging.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPackaging.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPackaging.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewPackaging.Name = "dataGridViewPackaging";
            this.dataGridViewPackaging.ReadOnly = true;
            this.dataGridViewPackaging.RowHeadersVisible = false;
            this.dataGridViewPackaging.Size = new System.Drawing.Size(188, 192);
            this.dataGridViewPackaging.TabIndex = 3;
            this.dataGridViewPackaging.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPackaging_CellClick);
            // 
            // groupBoxProduct
            // 
            this.groupBoxProduct.Controls.Add(this.dataGridViewProduct);
            this.groupBoxProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxProduct.Location = new System.Drawing.Point(403, 3);
            this.groupBoxProduct.Name = "groupBoxProduct";
            this.groupBoxProduct.Size = new System.Drawing.Size(194, 211);
            this.groupBoxProduct.TabIndex = 2;
            this.groupBoxProduct.TabStop = false;
            this.groupBoxProduct.Text = "Товар";
            // 
            // dataGridViewProduct
            // 
            this.dataGridViewProduct.AllowUserToAddRows = false;
            this.dataGridViewProduct.AllowUserToDeleteRows = false;
            this.dataGridViewProduct.AllowUserToResizeColumns = false;
            this.dataGridViewProduct.AllowUserToResizeRows = false;
            this.dataGridViewProduct.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewProduct.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewProduct.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewProduct.Name = "dataGridViewProduct";
            this.dataGridViewProduct.ReadOnly = true;
            this.dataGridViewProduct.RowHeadersVisible = false;
            this.dataGridViewProduct.Size = new System.Drawing.Size(188, 192);
            this.dataGridViewProduct.TabIndex = 3;
            this.dataGridViewProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProduct_CellClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewAddress);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(803, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(196, 211);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Куплено";
            // 
            // dataGridViewAddress
            // 
            this.dataGridViewAddress.AllowUserToAddRows = false;
            this.dataGridViewAddress.AllowUserToDeleteRows = false;
            this.dataGridViewAddress.AllowUserToResizeColumns = false;
            this.dataGridViewAddress.AllowUserToResizeRows = false;
            this.dataGridViewAddress.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewAddress.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewAddress.Name = "dataGridViewAddress";
            this.dataGridViewAddress.ReadOnly = true;
            this.dataGridViewAddress.RowHeadersVisible = false;
            this.dataGridViewAddress.Size = new System.Drawing.Size(190, 192);
            this.dataGridViewAddress.TabIndex = 4;
            this.dataGridViewAddress.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAddress_CellClick);
            // 
            // timerTimeToWait
            // 
            this.timerTimeToWait.Interval = 60000;
            this.timerTimeToWait.Tick += new System.EventHandler(this.timerTimeToWait_Tick);
            // 
            // contextMenuOrder
            // 
            this.contextMenuOrder.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuOrder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemSetStatusOrder});
            this.contextMenuOrder.Name = "contextMenuStrip1";
            this.contextMenuOrder.Size = new System.Drawing.Size(231, 26);
            // 
            // ToolStripMenuItemSetStatusOrder
            // 
            this.ToolStripMenuItemSetStatusOrder.Name = "ToolStripMenuItemSetStatusOrder";
            this.ToolStripMenuItemSetStatusOrder.Size = new System.Drawing.Size(230, 22);
            this.ToolStripMenuItemSetStatusOrder.Text = "Установить статус оплачено";
            this.ToolStripMenuItemSetStatusOrder.Click += new System.EventHandler(this.ToolStripMenuItemSetStatusOrder_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TelegramBotStore";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.groupBoxControl.ResumeLayout(false);
            this.groupBoxControl.PerformLayout();
            this.panelControl.ResumeLayout(false);
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrders)).EndInit();
            this.groupBoxUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).EndInit();
            this.groupBoxOrders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReview)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBoxCities.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCities)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRegions)).EndInit();
            this.groupBoxRegions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPackaging)).EndInit();
            this.groupBoxProduct.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProduct)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAddress)).EndInit();
            this.contextMenuOrder.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAdd;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSettings;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.GroupBox groupBoxControl;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelControl;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.GroupBox groupBoxUsers;
        private System.Windows.Forms.DataGridView dataGridViewUsers;
        private System.Windows.Forms.GroupBox groupBoxOrders;
        private System.Windows.Forms.DataGridView dataGridViewOrders;
        private System.Windows.Forms.Button buttonAuthorize;
        private System.Windows.Forms.Label labelQiwiBalance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBoxRegions;
        private System.Windows.Forms.GroupBox groupBoxCities;
        private System.Windows.Forms.DataGridView dataGridViewRegions;
        private System.Windows.Forms.DataGridView dataGridViewCities;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewPackaging;
        private System.Windows.Forms.GroupBox groupBoxProduct;
        private System.Windows.Forms.DataGridView dataGridViewProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewReview;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAddCity;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAddRegiob;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAddProduct;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAddPackaging;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridViewAddress;
        private System.Windows.Forms.ToolStripMenuItem адресToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSending;
        private System.Windows.Forms.Timer timerTimeToWait;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemMenu;
        private System.Windows.Forms.ToolStripMenuItem ALLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьКоментарийToolStripMenuItem;
        private System.Windows.Forms.Button buttonRefreshComment;
        private System.Windows.Forms.ContextMenuStrip contextMenuOrder;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSetStatusOrder;
    }
}

