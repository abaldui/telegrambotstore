﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelegramBotStore
{
    public partial class EditKey : Form
    {
        public EditKey()
        {
            InitializeComponent();
        }

        private void EditKey_Load(object sender, EventArgs e)
        {
            textBoxId.Text = CommonWrapper.KeyGen.GetProcessorID();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Key = textBoxKey.Text;
            Properties.Settings.Default.Save();

            //if (!Singleton.Main.CheckKey())
            //    MessageBox.Show("Неверный ключ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //else
            //    this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void EditKey_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (!Singleton.Main.CheckKey())
            //    Application.Exit();
        }
    }
}
