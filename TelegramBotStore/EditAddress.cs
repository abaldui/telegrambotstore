﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TelegramBotStore.Domain;
using TelegramBotStore.Domain.DbContext;

namespace TelegramBotStore
{
    public partial class EditAddress : Form
    {
        private readonly ILog log = LogManager.GetLogger("TelegramBotSushi.EditAddress.cs");

        private BotContext context;
        private Package package;
        private Address address;

        private long cityId;
        private long regionId;
        private long productId;
        private long packageId;
        private long? addressId;

        public EditAddress(long _cityId, long _regionId, long _productId, long _packageId, long? _addressId)
        {
            InitializeComponent();

            cityId = _cityId;
            regionId = _regionId;
            productId = _productId;
            packageId = _packageId;
            addressId = _addressId;
        }

        private void EditAddress_Load(object sender, EventArgs e)
        {
            context = BotContext.Create();

            City city = context.Cities.FirstOrDefault(x => x.Id == cityId);
            Domain.Region region = city.Regions.FirstOrDefault(x => x.Id == regionId);
            Product product = region.Products.FirstOrDefault(x => x.Id == productId);
            package = product.Packages.FirstOrDefault(x => x.Id == packageId);

            if (addressId.HasValue)
            {
                address = package.Addresses.FirstOrDefault(x => x.Id == addressId.Value);
                textBoxName.Text = address.Name;
            }

            this.Text = addressId.HasValue ? "Редактирование куплено" : "Добавление куплено";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxName.Text))
                return;

            if (!addressId.HasValue && package.Addresses.Any(x => x.Name == textBoxName.Text))
            {
                MessageBox.Show("куплено с таким название уже добавлен", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                if (!addressId.HasValue)
                {
                    string[] separator = new string[] { "\r\n" };

                    string[] splits = textBoxName.Text.Split(separator, StringSplitOptions.None);

                    foreach (string split in splits)
                    {
                        if (!package.Addresses.Any(x => x.Name == split)) package.Addresses.Add(new Address(split));
                    }
                }
                else
                    address.Name = textBoxName.Text;

                context.SaveChanges();

                Singleton.Main.InitAddresses();

                this.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
